export class TranslationUtils {
  public static vocabularyType(key: string, type?: 'name' | 'tooltip') {
    return `VocabularyType.${key}${type ? ['.', type].join('') : ''}`;
  }
}

export function translationOrValue(value: string, key: string, tFn) {
  const translated = tFn(key);
  return !!translated && translated !== key ? translated : value;
}

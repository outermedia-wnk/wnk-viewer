// import original module declarations
import 'styled-components';

// and extend them!
declare module 'styled-components' {
  export interface DefaultTheme {
    breakpoints: {
      mobile: number;
      tablet_portrait: number;
      tablet_landscape: number;
      desktop: number;
    };
    grid: {
      gutter: {
        mobile: number;
        tablet_portrait: number;
        // tablet_landscape: number;
        desktop: number;
      };
    };
    pagePadding: {
      mobile: number;
      tablet_portrait: number;
      // tablet_landscape: number;
      desktop: number;
    };
    spacing: {
      [key: string]: numbers;
    };
    baseFontSize: number;
    font: {
      family: string;
      weight: {
        light: number;
        regular: number;
        bold: number;
      };
      size: {
        [key: string]: number;
      };
    };
    color: {
      [key: string]: string;
    };
  }
}

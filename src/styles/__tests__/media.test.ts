import { css } from 'styled-components/macro';
import { media } from '../media';
import { defaultTheme } from '../defaultTheme';

export const { breakpoints } = defaultTheme;

describe('media', () => {
  it('should return media query in css', () => {
    const mediaQuery = media.mobile`color:red;`.join('');
    const cssVersion = css`
      @media only screen and (max-width: ${breakpoints.tablet_portrait - 1}px) {
        color: red;
      }
    `.join('');
    expect(mediaQuery).toEqual(cssVersion);
  });
});

import { AuthState } from '../app/features/Auth';
import { ConfigState } from '../app/features/Config';
import { ContentViewSwitcherState } from '../app/features/ContentViewSwitcher';
import { MobileMenuState } from '../app/features/MobileMenu';
import { SearchState } from '../app/features/Search';
import { TreeState } from '../app/features/Tree';

/*
  Because the redux-injectors injects your reducers asynchronously somewhere in your code
  You have to declare them here manually
*/
export interface RootState {
  // [INSERT NEW REDUCER KEY ABOVE] < Needed for generating containers seamlessly
  readonly auth: AuthState;
  readonly config: ConfigState;
  readonly contentViewSwitcher: ContentViewSwitcherState;
  readonly mobileMenu: MobileMenuState;
  readonly search: SearchState;
  readonly tree: TreeState;
}

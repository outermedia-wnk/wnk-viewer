export interface Link {
  url: string;
  label: string;
}

export type MenuItem = string | Link;

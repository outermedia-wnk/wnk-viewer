import de from 'date-fns/locale/de';
import formatWithOptions from 'date-fns/fp/formatWithOptions';

export class Helper {
  public static capitalize(s: string) {
    return `${s.charAt(0).toUpperCase()}${s.slice(1)}`;
  }

  public static DateFormat = {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric',
  };
  public static formatDate(date: Date, formatStyle: string = 'dd-MM-yyyy') {
    const dateToString = formatWithOptions({ locale: de }, formatStyle);
    return dateToString(date);
  }

  public static formatNumber(value: number): string {
    return new Intl.NumberFormat('de-DE').format(value);
  }

  // public static convertLineBreaksToHtml(value: string): string {
  //   return value.replace(/[\n|\r]+/g, '<br/>');
  // }
  //
}

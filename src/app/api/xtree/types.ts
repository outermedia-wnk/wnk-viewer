/** XTree query options */
export interface XTreeQueryParams {
  count?: string;
  direction?: string;
  homonymLexicalValue?: string;
  jsonFull?: string;
  lang?: string;
  level?: string;
  mode?: string;
  nodeId?: string;
  restrictedTo?: string;
  searchFields?: string;
  searchIdsList?: string;
  searchTermsList?: string;
  start?: string;
  typeofVocItem?: string;
  vocabulary?: string;
  id?: string;
  sortList?: string;
  public?: number;
}

export interface TreeNode {
  id: string;
  entityType: string;
  preferredTerm: string;
  qualifier?: string;
  lang: string;
  parentIds: string[];
  childrenIds: string[];
  superordinateRelation: { id: string; typeOfHierarchicalRelation?: string }[];
  subordinateRelation: { id: string; typeOfHierarchicalRelation?: string }[];
}

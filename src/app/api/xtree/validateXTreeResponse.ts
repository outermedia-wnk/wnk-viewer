export function validateXTreeResponse(response) {
  return !!response && response.hasOwnProperty('vocItemCount');
}

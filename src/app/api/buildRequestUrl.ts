import { select } from 'redux-saga/effects';
import { selectApiUrl } from '../features/Config';

export function* buildRequestUrl(endpoint: string, query: string = '') {
  const apiUrl = yield select(selectApiUrl);
  const path = [apiUrl, endpoint].join('/');
  return [path, query].filter((part: string) => !!part).join('?');
}

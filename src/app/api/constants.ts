export const ENDPOINT = {
  SearchByTerm: 'getSearchVocItemsByTerm_sortList',
  SearchById: 'getSearchVocItemsById',
  Login: 'login.php',
  Logout: 'logout.php',
  Hierarchy: 'getFetchHierarchy',
};

import { VocabTypes } from './normalizer';

export function isStructureElement(entityType: string) {
  return [
    VocabTypes.FacetNode.toString(),
    VocabTypes.HierarchyNode.toString(),
    VocabTypes.NonIndexingConcept.toString(),
    VocabTypes.ThesaurusArray.toString(),
  ].includes(entityType);
}

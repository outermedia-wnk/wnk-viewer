import { XTreeTerm } from '../normalizer';

export interface Term extends XTreeTerm {
  sortKey: string;
}
const langWeight = {
  de: '0001',
  en: '0002',
};
const labelRoleWeight = {
  prefLabel: '0001',
  altLabel: '0002',
  default: '9999',
};
const grammaticalNumberWeight = {
  singular: '0001',
  plural: '0002',
};

export class Terms {
  protected _termIds: string[] = [];
  protected _termsById: Record<string, XTreeTerm> = {};
  protected _sortedTerms: Term[] = [];

  public constructor(ids: string[], terms: { [id: string]: XTreeTerm } = {}) {
    this._termIds = ids;
    this._termsById = terms;
    this.init();
  }

  protected init() {
    this._sortedTerms = this._termIds
      .map((id: string) => {
        const term: Term = {
          ...this._termsById[id],
          sortKey: this.resolveSortKey(this._termsById[id]),
        };
        return term;
      })
      .sort((a: Term, b: Term) => this.compareTerms(a, b));
  }

  /** Sortierung:
   * 1. die bevorzugte deutsche Bezeichnung
   * 2. der zugehörige Plural
   * 3. alle weiteren deutschen Bezeichnungen alphabetisch, jeweils mit zugehörigem Plural
   * 4. die bevorzugte englische Bezeichnung
   * 5. weitere Sprachen alphabetisch
   */
  protected resolveSortKey(term: XTreeTerm) {
    const parts: string[] = [];
    langWeight[term.lang]
      ? parts.push(langWeight[term.lang])
      : parts.push(term.lang.padStart(4, '0'));
    labelRoleWeight[term.labelRole]
      ? parts.push(labelRoleWeight[term.labelRole])
      : parts.push(labelRoleWeight.default);
    parts.push(term.Term);
    parts.push(grammaticalNumberWeight[term.grammaticalNumber]);
    return parts.join(';');
  }

  protected compareTerms(termA: Term, termB: Term): number {
    if (termA.sortKey < termB.sortKey) {
      return -1;
    }
    if (termA.sortKey > termB.sortKey) {
      return 1;
    }
    if (termA.Term < termB.Term) {
      return -1;
    }
    if (termA.Term > termB.Term) {
      return 1;
    }
    return 0;
  }

  get terms(): Term[] {
    return this._sortedTerms;
  }
}

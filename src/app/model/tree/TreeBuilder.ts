import { TreeNode } from '../../api/xtree';
import { NodesMap } from '../../features/Tree';
import { NodeInspector } from './NodeInspector';

export class TreeBuilder {
  protected _nodesMap: NodesMap;
  protected _nodesList: TreeNode[];
  protected _roots: string[] = [];

  public constructor(nodesMap: NodesMap) {
    this._nodesMap = nodesMap;
    this._nodesList = this.mapToList(nodesMap);
    this._roots = [NodeInspector.getRootNode()];
  }

  protected mapToList(nodesMap: NodesMap) {
    return Object.keys(nodesMap).reduce((acc: TreeNode[], id: string) => {
      acc.push(nodesMap[id]);
      return acc;
    }, []);
  }

  get roots(): string[] {
    return this._roots;
  }

  public getNodeById(id: string): TreeNode {
    return this._nodesMap[id];
  }

  public getNodePath(node: TreeNode): string[] {
    return NodeInspector.getNodePath(node);
  }

  public hasNode(id: string) {
    return this._nodesMap.hasOwnProperty(id);
  }

  public sortIdsAlphabetically(ids: string[]): string[] {
    const idsByTerm = ids
      .filter((id: string) => this.hasNode(id))
      .reduce((acc: Record<string, string>, id: string) => {
        acc[this.getNodeById(id).preferredTerm] = id;
        return acc;
      }, {});
    return Object.keys(idsByTerm)
      .sort()
      .map((term: string) => idsByTerm[term]);
  }
}

import {
  normalizedRootNodes,
  rootNodesMap,
} from '../__test_samples__/normalizedRootNodes';
import { expectedTree, mapForTree } from '../__test_samples__/expectedTree';
import { initNodesMapFromNormalizedResponse } from '../normalizerToNodeUtils';
import { NodesMap } from '../../../features/Tree';

describe('Tree', () => {
  it('inits map from normalizerResponse', () => {
    const map: NodesMap = initNodesMapFromNormalizedResponse(
      normalizedRootNodes as any,
    );
    expect(map).toEqual(rootNodesMap);
  });

  it('populates children 1', () => {
    const nodesMap = mapForTree;
    const rootId = 'wk000249';
    const tree = mapToTree(nodesMap, rootId);
    expect(tree).toEqual(expectedTree);
  });
});

function mapToTree(nodesMap, id: string) {
  const node = nodesMap[id];
  if (node.childrenIds.length > 0) {
    return {
      name: node.preferredTerm,
      children: node.childrenIds.map(cid => mapToTree(nodesMap, cid)),
    };
  }
  return { name: node.preferredTerm };
}

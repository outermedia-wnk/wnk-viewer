import { NodesMap } from '../../../features/Tree';
import { VocabTypes } from '../../normalizer';
import {
  createHierarchy,
  getHierarchyIdsByRelationType,
  HierarchyRelation,
  hierarchyStrToArray,
} from '../hierarchyBuilder';
import { haengebahn } from '../__test_samples__/haengebahn.test';

const dummy: any = {
  preferredTerm: 'a',
  entityType: VocabTypes.Concept,
  lang: 'de',
  parentIds: [],
  childrenIds: [],
  superordinateRelation: [],
  subordinateRelation: [],
  isRoot: false,
};

const sample: NodesMap = {
  wk000249: { ...dummy, id: 'wk000249', parentIds: [] }, //preferredTerm: 'Oberste Ebene des WNK',
  wk01: { ...dummy, id: 'wk01', parentIds: ['wk000249'] }, //preferredTerm: 'Objekt',
  wk02: { ...dummy, id: 'wk02', parentIds: ['wk01'] }, //preferredTerm: 'System',
  wk03: { ...dummy, id: 'wk03', parentIds: ['wk02'] }, //preferredTerm: 'Hängebahn',
  wk04: { ...dummy, id: 'wk04', parentIds: ['wk02'] }, //preferredTerm: 'Seilbahn',
  wk05: { ...dummy, id: 'wk05', parentIds: ['wk03', 'wk04'] }, //preferredTerm: 'Luftseilbahn',
  wk06: { ...dummy, id: 'wk06', parentIds: ['wk05'] }, //preferredTerm: 'Sesselbahn',
  wk07: { ...dummy, id: 'wk07', parentIds: ['wk05', 'wk06'] }, //preferredTerm: 'aaa',
  wk08: { ...dummy, id: 'wk08', parentIds: ['wk07'] }, //preferredTerm: 'bbb',
};

describe('Hierarchy', () => {
  it('build polyhierarchy for Haengebahn', () => {
    const result = createHierarchy(haengebahn.id, haengebahn.nodesMap);
    expect(result).toEqual(haengebahn.hierarchy);
  });

  it('build polyhierarchy 1', () => {
    const nodesMap: NodesMap = sample;
    const id = 'wk06';
    const expected = [
      ';wk06;wk05;wk03;wk02;wk01;wk000249',
      ';wk06;wk05;wk04;wk02;wk01;wk000249',
    ];
    const result = createHierarchy(id, nodesMap);
    expect(result).toEqual(expected);
  });

  it('build polyhierarchy 2', () => {
    const nodesMap: NodesMap = sample;
    const id = 'wk08';
    const expected = [
      ';wk08;wk07;wk05;wk03;wk02;wk01;wk000249',
      ';wk08;wk07;wk05;wk04;wk02;wk01;wk000249',
      ';wk08;wk07;wk06;wk05;wk03;wk02;wk01;wk000249',
      ';wk08;wk07;wk06;wk05;wk04;wk02;wk01;wk000249',
    ];
    const result = createHierarchy(id, nodesMap);
    expect(result).toEqual(expected);
  });

  it('hierarchyStrToArray', () => {
    const input = [
      ';wk06;wk05;wk03;wk02;wk01;wk00',
      ';wk06;wk05;wk04;wk02;wk01;wk00',
    ];
    const expected = [
      ['wk00', 'wk01', 'wk02', 'wk03', 'wk05', 'wk06'],
      ['wk00', 'wk01', 'wk02', 'wk04', 'wk05', 'wk06'],
    ];
    const result = hierarchyStrToArray(input);
    expect(result).toEqual(expected);
  });

  it('getHierarchyItemsByType G', () => {
    const input = [
      { id: 'wk001' },
      { id: 'wk002', typeOfHierarchicalRelation: 'btg_ntg' },
      { id: 'wk003' },
      { id: 'wk004', typeOfHierarchicalRelation: 'btg_ntg' },
      { id: 'wk005', typeOfHierarchicalRelation: 'btp_ntp' },
    ];
    const expected = ['wk001', 'wk002', 'wk003', 'wk004'];
    const result = getHierarchyIdsByRelationType(
      input,
      HierarchyRelation.Generic,
      [],
    );
    expect(result).toEqual(expected);
  });

  it('getHierarchyItemsByType G', () => {
    const input = [
      { id: 'wk001' },
      { id: 'wk002', typeOfHierarchicalRelation: 'btg_ntg' },
      { id: 'wk003' },
      { id: 'wk004', typeOfHierarchicalRelation: 'btg_ntg' },
      { id: 'wk005', typeOfHierarchicalRelation: 'btp_ntp' },
    ];
    const expected = ['wk005'];
    const result = getHierarchyIdsByRelationType(
      input,
      HierarchyRelation.Partitive,
      [],
    );
    expect(result).toEqual(expected);
  });
});

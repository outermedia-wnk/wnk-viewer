import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { PageLayout } from '../../components/PageLayout';
import { NotFound } from '../../components/Page/NotFound';

export function NotFoundPage() {
  const { t } = useTranslation('helmet');
  return (
    <>
      <Helmet>
        <title>{t('helmet.notFound.title')}</title>
        <meta name="description" content={t('helmet.notFound.description')} />
      </Helmet>
      <PageLayout>
        <NotFound />
      </PageLayout>
    </>
  );
}

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { PageLayout } from '../../components/PageLayout';
import { Help } from '../../components/Page/Help';

export function HelpPage() {
  const { t } = useTranslation('helmet');
  return (
    <>
      <Helmet>
        <title>{t('helmet.help.title')}</title>
        <meta name="description" content={t('helmet.help.description')} />
      </Helmet>
      <PageLayout>
        <Help />
      </PageLayout>
    </>
  );
}

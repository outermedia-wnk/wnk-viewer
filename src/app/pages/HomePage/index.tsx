import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { PageLayout } from '../../components/PageLayout';
import { Home } from '../../components/Page/Home';

export function HomePage() {
  const { t } = useTranslation('helmet');
  return (
    <>
      <Helmet>
        <title>{t('helmet.homepage.title')}</title>
        <meta name="description" content={t('helmet.homepage.description')} />
      </Helmet>
      <PageLayout>
        <Home />
      </PageLayout>
    </>
  );
}

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { PageLayout } from '../../components/PageLayout';
import { Team } from '../../components/Page/Team';

export function TeamPage() {
  const { t } = useTranslation('helmet');
  return (
    <>
      <Helmet>
        <title>{t('helmet.team.title')}</title>
        <meta name="description" content={t('helmet.team.description')} />
      </Helmet>
      <PageLayout>
        <Team />
      </PageLayout>
    </>
  );
}

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { PageLayout } from '../../components/PageLayout';
import { Datenschutz } from '../../components/Page/Datenschutz';

export function DatenschutzPage() {
  const { t } = useTranslation('helmet');
  return (
    <>
      <Helmet>
        <title>{t('helmet.datenschutz.title')}</title>
        <meta
          name="description"
          content={t('helmet.datenschutz.description')}
        />
      </Helmet>
      <PageLayout>
        <Datenschutz />
      </PageLayout>
    </>
  );
}

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { PageLayout } from '../../components/PageLayout';
import { About } from '../../components/Page/About';

export function AboutPage() {
  const { t } = useTranslation('helmet');
  return (
    <>
      <Helmet>
        <title>{t('helmet.about.title')}</title>
        <meta name="description" content={t('helmet.about.description')} />
      </Helmet>
      <PageLayout>
        <About />
      </PageLayout>
    </>
  );
}

import * as React from 'react';
import { PageLayout } from '../../components/PageLayout';
import { Search } from '../../components/Page/Search';

export function ResultPage() {
  return (
    <PageLayout>
      <Search />
    </PageLayout>
  );
}

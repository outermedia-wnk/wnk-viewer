import React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { VocabularyItem } from '../../model/vocabularyItems/VocabularyItem';
import { useSelector } from 'react-redux';
import { selectNodeById } from '../../features/Tree';
import { StringUtils } from '../../model/StringUtils';

interface Props {
  vocab: VocabularyItem;
}

export function ResultPageHelmet(props: Props) {
  const { t } = useTranslation('helmet');
  const { t: t2 } = useTranslation();
  const node = useSelector(selectNodeById(props.vocab.id));

  let title: string = node
    ? StringUtils.termWithQualifier(node.preferredTerm, node.qualifier)
    : props.vocab.title;
  title = node
    ? StringUtils.termWithEntityType(title, node.entityType, t2)
    : props.vocab.type;
  return (
    <Helmet>
      <title>{t('helmet.searchResult.title', { title })}</title>
      <meta name="description" content={t('helmet.searchResult.description')} />
    </Helmet>
  );
}

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { PageLayout } from '../../components/PageLayout';
import { Accessibility } from '../../components/Page/Accessibility';

export function AccessibilityPage() {
  const { t } = useTranslation('helmet');
  return (
    <>
      <Helmet>
        <title>{t('helmet.accessibility.title')}</title>
        <meta
          name="description"
          content={t('helmet.accessibility.description')}
        />
      </Helmet>
      <PageLayout>
        <Accessibility />
      </PageLayout>
    </>
  );
}

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { PageLayout } from '../../components/PageLayout';
import { Impressum } from '../../components/Page/Impressum';

export function ImpressumPage() {
  const { t } = useTranslation('helmet');
  return (
    <>
      <Helmet>
        <title>{t('helmet.impressum.title')}</title>
        <meta name="description" content={t('helmet.impressum.description')} />
      </Helmet>
      <PageLayout>
        <Impressum />
      </PageLayout>
    </>
  );
}

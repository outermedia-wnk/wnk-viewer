import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { PageLayout } from '../../components/PageLayout';
import { Search } from '../../components/Page/Search';

export function SearchPage() {
  const { t } = useTranslation('helmet');
  return (
    <>
      <Helmet>
        <title>{t('helmet.search.title')}</title>
        <meta name="description" content={t('helmet.search.description')} />
      </Helmet>
      <PageLayout>
        <Search />
      </PageLayout>
    </>
  );
}

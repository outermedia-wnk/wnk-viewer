import React from 'react';
import { useSelector } from 'react-redux';
import { selectResultById } from '../../../features/Search';
import { StringUtils } from '../../../model/StringUtils';
import { PermalinkBuilder } from '../../../routes/PermalinkBuilder';
import { ResultLink } from './style';

interface LocalProps {
  id: string;
}
type Props = LocalProps;

export function ResultItem(props: Props) {
  const item: any = useSelector(selectResultById(props.id));
  const title = StringUtils.termWithQualifier(
    item.preferredTerm,
    item.qualifier,
  );
  const url = PermalinkBuilder.build(item.id);

  const entityType = item.vocabTypeQualifier
    ? `(${item.vocabTypeQualifier})`
    : '';

  return (
    <ResultLink href={url}>
      <span>{StringUtils.joinNonEmptyStrings([title, entityType], ' ')}</span>
    </ResultLink>
  );
}

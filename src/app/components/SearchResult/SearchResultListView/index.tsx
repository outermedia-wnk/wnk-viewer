import React from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import {
  selectResultList,
  selectSearchIsLoading,
} from '../../../features/Search';
import { Routes } from '../../../routes';
import { HiddenH1 } from '../../HTML';
import { IconClear } from '../../Icon';
import { ResultItem } from './ResultItem';
import { messages } from './messages';
import { TotalResults, ResultList, ResultListItem, ResetButton } from './style';

export function SearchResultListView() {
  const { t } = useTranslation();

  const list: any[] = useSelector(selectResultList);
  const isLoading = useSelector(selectSearchIsLoading);

  return (
    <>
      <HiddenH1 aria-label={t(messages.searchResultsList)} />

      {!isLoading && (
        <TotalResults>
          <span>{t(messages.totalResultsLabel, { hits: list.length })}</span>
          <ResetButton
            aria-label={t(messages.resetButtonAriaLabel)}
            aria-hidden={true}
            title={t(messages.resetButtonAriaLabel)}
            to={{ pathname: Routes.Search, search: '' }}
          >
            <IconClear />
          </ResetButton>
        </TotalResults>
      )}

      <ResultList>
        {list.map((id: string) => (
          <ResultListItem key={id}>
            <ResultItem id={id} />
          </ResultListItem>
        ))}
      </ResultList>
    </>
  );
}

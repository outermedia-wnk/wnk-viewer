import { _t } from '../../../../utils/messages';

const scope = 'Search.SearchResultsList';

export const messages = {
  searchResultsList: _t('Page.Search.SearchResults.H1.ariaLabel'),
  totalResultsLabel: _t(`${scope}.totalResultsLabel`),
  resetButtonAriaLabel: _t(`${scope}.resetButtonAriaLabel`),
  resetButtonTooltip: _t(`${scope}.resetButtonTooltip`),
};

import styled from 'styled-components/macro';
import { font_ResultsTotal, font_ResultLink, media } from '../../../../styles';
import { LI, UL } from '../../HTML';
import { Link, RouterLink } from '../../Link';

export const TotalResults = styled.div`
  ${font_ResultsTotal};
  margin-bottom: ${props => props.theme.spacing._34}rem;
  display: flex;
  justify-content: space-between;
`;

export const HR = styled.div`
  width: 80%;
  height: 0px;
  border-top: 1px solid ${props => props.theme.color.line_gray};
  margin-top: ${props => props.theme.spacing._16}rem;
  margin-bottom: ${props => props.theme.spacing._24}rem;
`;

export const ResetButton = styled(RouterLink)`
  color: ${props => props.theme.color.white};
  &:hover {
    color: ${props => props.theme.color.white};
  }
`;

export const ResultList = styled(UL)``;

export const ResultListItem = styled(LI)`
  display: flex;
  flex-flow: row wrap;
  align-items: center;

  svg {
    margin-left: ${props => props.theme.spacing._10}rem;
  }
`;

export const ResultLink = styled(Link)`
  margin-bottom: ${props => props.theme.spacing._12}rem;
  ${font_ResultLink};
  text-decoration: none;

  ${media.tablet_portrait_up`
    &:hover {
      color: ${props => props.theme.color.white};
    }
  `}
`;

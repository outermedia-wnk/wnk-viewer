import styled, { css } from 'styled-components/macro';
import {
  font_Body,
  font_BodyLight,
  font_CTA,
  font_CTA_Link,
  media,
} from '../../../../styles';
import { H1, PLight, SectionHeader } from '../../HTML';
import { Link, linkInteractionStyles } from '../../Link';

export const Header = styled(SectionHeader)`
  margin-bottom: ${props => props.theme.spacing._8}rem;
  display: flex;
  align-items: center;
  > a {
    margin-left: ${props => props.theme.spacing._6}rem;
    cursor: help;
    display: flex;
    align-items: center;
  }
`;

export const TermType = styled(Header)`
  margin-bottom: ${props => props.theme.spacing._10}rem;
  > a {
    cursor: help;
  }
`;

export const Term = styled(H1)`
  margin-bottom: ${props => props.theme.spacing._48}rem;
`;

export const Section = styled.section`
  ${font_BodyLight};
  margin-bottom: ${props => props.theme.spacing._40}rem;
`;

export const InlineHeader = styled(SectionHeader)`
  display: inline-block;
  margin-right: ${props => props.theme.spacing._12}rem;
`;

export const Label = styled.label`
  ${font_BodyLight};
  display: inline-block;
  margin-right: ${props => props.theme.spacing._12}rem;
`;

export const Box = styled(PLight)`
  max-width: 500px;
  margin: 0;
`;

export const Table = styled.div`
  border-bottom: 1px solid ${props => props.theme.color.line_gray};

  ${media.mobile_up`
    display: -ms-grid;
    display: grid;
    grid-template-rows: auto;
    -ms-grid-rows: auto;
    grid-template-columns: auto minmax(50%, 1fr);
    -ms-grid-columns: auto 0 minmax(50%, 1fr);
  `};
`;

const tableItem = css`
  display: block;
  &:nth-child(2n-1) {
    border-top: 1px solid ${props => props.theme.color.line_gray};
  }
  ${media.mobile_up`
    border-top: 1px solid ${props => props.theme.color.line_gray};
  `}
`;
const tableItemRight = css`
  ${tableItem};
  padding-bottom: ${props => props.theme.spacing._6}rem;
  ${media.mobile_up`
    padding-top: ${props => props.theme.spacing._6}rem;
  `};
`;
const tableItemLeft = css`
  ${tableItem};
  padding-right: ${props => props.theme.spacing._10}rem;
  padding-top: ${props => props.theme.spacing._6}rem;
  ${media.mobile_up`
    padding-bottom: ${props => props.theme.spacing._6}rem;
  `};
`;

export const DesignationTerm = styled.div`
  ${tableItemLeft};
  ${font_Body};
`;
export const Designation = styled.div`
  ${tableItemRight};
  ${font_BodyLight};
`;

export const TermRelation = styled.label`
  ${tableItemLeft};
  ${font_BodyLight};
`;
export const TermLink = styled(Link)`
  ${tableItemRight};
`;

export const CTASection = styled(Section)`
  background-color: ${props => props.theme.color.cta_box_gray};
  padding: ${props => props.theme.spacing._42}rem
    ${props => props.theme.spacing._16}rem
    ${props => props.theme.spacing._58}rem;

  > div {
    margin: 0 auto;
    max-width: 400px;
  }
  span {
    display: block;
    ${font_CTA};
  }
  a {
    display: block;
    ${font_CTA_Link};
    ${linkInteractionStyles};
    text-decoration: underline;
  }
`;

export const GridSection = styled(Section)`
  display: -ms-grid;
  display: grid;
  -ms-grid-rows: auto;
  grid-template-rows: auto;
  -ms-grid-columns: auto 0 1fr;
  grid-column-gap: 0;
  grid-template-columns: auto 1fr;
`;

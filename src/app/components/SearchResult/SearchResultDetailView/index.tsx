import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import teamContent from '../../../../content/team.json';
import { dateMessages } from '../../../../locales/common/dateMessages';
import { TranslationUtils } from '../../../../locales/TranslationUtils';
import { selectNormalizedData } from '../../../features/Search';
import { useSelectedVocab } from '../../../hooks/useSelectedVocab';
import { VocabularyItem } from '../../../model/vocabularyItems/VocabularyItem';
import { ResultPageHelmet } from '../../../pages/ResultPage/ResultPageHelmet';
import { PermalinkBuilder } from '../../../routes/PermalinkBuilder';
import { IconInfo } from '../../Icon';
import { Link } from '../../Link';
import { TermWithQualifierOrEntityType } from '../../TermWithQualifierOrEntityType';
import { messages } from './messages';
import { Definitions } from './Sections/Definitions';
import { Designations } from './Sections/Designations';
import { Explanation } from './Sections/Explanation';
import { Hierarchies } from './Sections/Hierarchies';
import { Mapping } from './Sections/Mapping';
import { RelatedTerms } from './Sections/RelatedTerms';
import { RelatedTermsLoader } from './Sections/RelatedTerms/RelatedTermsLoader';
import { Status } from './Sections/Status';
import { UsageNotes } from './Sections/UsageNotes';
import { VocabImages } from './Sections/VocabImages';
import {
  CTASection,
  GridSection,
  InlineHeader,
  Label,
  Section,
  TermType,
  Term,
} from './style';

export function SearchResultDetailView() {
  const { t } = useTranslation();
  const vocab: VocabularyItem | null = useSelectedVocab();
  const normalizedData = useSelector(selectNormalizedData);

  if (
    !vocab ||
    !normalizedData ||
    !normalizedData.entities ||
    !normalizedData.entities.term
  ) {
    return null;
  }

  let lastUpdateDate;
  try {
    lastUpdateDate = t(dateMessages.formattedIntlDate, {
      date: new Date(vocab.lastUpdated),
    });
  } catch (err) {
    lastUpdateDate = vocab.lastUpdated;
  }

  const permalink = PermalinkBuilder.buildURI(vocab.id);

  return (
    <>
      <ResultPageHelmet vocab={vocab} />
      <div>
        <TermType id="term-type" as="div">
          {t(TranslationUtils.vocabularyType(vocab.type, 'name'))}
          <a href="#term-type">
            <IconInfo
              title={t(TranslationUtils.vocabularyType(vocab.type, 'tooltip'))}
            />
          </a>
        </TermType>
        <Term>
          <TermWithQualifierOrEntityType id={vocab.id} vocab={vocab} />
        </Term>

        <Section aria-labelledby="header-uri">
          <InlineHeader id="header-uri">{t(messages.uri)}</InlineHeader>
          <Link href={permalink}>{permalink}</Link>
        </Section>

        <Definitions vocab={vocab} />
        <Designations vocab={vocab} />
        <VocabImages vocab={vocab} />
        <UsageNotes vocab={vocab} />
        <Explanation vocab={vocab} />
        <RelatedTermsLoader vocab={vocab}>
          <RelatedTerms vocab={vocab} />
        </RelatedTermsLoader>
        <Hierarchies vocab={vocab} />
        <Mapping vocab={vocab} />

        <CTASection aria-labelledby="cta-header">
          <div>
            <span id="cta-header">{t(messages['cta.part1'])}</span>
            <a href={`mailto:${teamContent.wnkTeamAddress.mail}`}>
              {t(messages['cta.part2'])}
            </a>
          </div>
        </CTASection>

        <GridSection>
          <Label id="label-last-update">{t(messages.last_update)} </Label>
          <span aria-labelledby="label-last-update">{lastUpdateDate}</span>

          <Status vocab={vocab} />
        </GridSection>
      </div>
    </>
  );
}

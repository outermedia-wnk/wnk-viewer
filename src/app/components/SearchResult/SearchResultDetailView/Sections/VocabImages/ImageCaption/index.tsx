import React from 'react';
import { useTranslation } from 'react-i18next';
import { VocabImage } from '../../../../../../model/vocabularyItems/VocabImage';
import { messages } from '../../../messages';

interface Props {
  vocabImage: VocabImage;
}

export function ImageCaption(props: Props) {
  const { t } = useTranslation();

  const parts: JSX.Element[] = [];
  parts.push(<span>{props.vocabImage.title}</span>);

  if (props.vocabImage.photographer) {
    parts.push(
      <span>
        {t(messages.photographer)}: {props.vocabImage.photographer}
      </span>,
    );
  }

  if (props.vocabImage.copyrights) {
    parts.push(
      <span>
        {t(messages.copyrights)}: {props.vocabImage.copyrights}
      </span>,
    );
  }

  if (props.vocabImage.licenseUrl) {
    parts.push(
      <span>
        {t(messages.license)}:{' '}
        <a href={props.vocabImage.licenseUrl}>{props.vocabImage.licenseUrl}</a>
      </span>,
    );
  }

  if (!!props.vocabImage.source) {
    parts.push(
      <span>
        {t(messages.source)}: {props.vocabImage.source}
      </span>,
    );
  }

  return (
    <>
      {parts.map((part: any, idx: number) => (
        <React.Fragment key={`caption-part-${idx}`}>
          {part}
          {parts.length > idx + 1 && <span>, </span>}
        </React.Fragment>
      ))}
    </>
  );
}

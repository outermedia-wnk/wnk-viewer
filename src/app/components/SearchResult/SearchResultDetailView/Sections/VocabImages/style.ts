import styled from 'styled-components/macro';
import { font_ImageCaption } from '../../../../../../styles';
import { linkInteractionStyles } from '../../../../Link';

export const ImageContainer = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${props => props.theme.color.bg_gray};
`;

export const Figure = styled.figure`
  display: flex;
  flex-direction: column;
  flex-grow: 0;
  align-items: center;
  margin: 0;

  img {
    width: 100%;
    height: auto;
    max-height: ${props => props.theme.spacing._350}rem;
  }
`;

export const FigCaption = styled.figcaption`
  ${font_ImageCaption};
  background-color: ${props => props.theme.color.white};
  padding-top: ${props => props.theme.spacing._6}rem;
  a {
    ${font_ImageCaption};
    ${linkInteractionStyles};
  }
`;

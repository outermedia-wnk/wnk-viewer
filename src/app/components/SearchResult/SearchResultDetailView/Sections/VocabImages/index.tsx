import React from 'react';
import { useTranslation } from 'react-i18next';
import { VocabularyItem } from '../../../../../model/vocabularyItems/VocabularyItem';
import { VocabImage } from '../../../../../model/vocabularyItems/VocabImage';
import { HiddenH2 } from '../../../../HTML';
import { Section } from '../../style';
import { ImageContainer, FigCaption, Figure } from './style';
import { ImageCaption } from './ImageCaption';

interface Props {
  vocab: VocabularyItem;
}

export function VocabImages(props: Props) {
  const { t } = useTranslation();
  if (!props.vocab.vocabImages) {
    return null;
  }

  return (
    <Section aria-labelledby="header-image">
      <HiddenH2
        id="header-image"
        aria-label={t('Page.ResultDetails.headers.image')}
      />
      <ImageContainer>
        {props.vocab.vocabImages.map((image: VocabImage, index) => {
          return (
            <React.Fragment key={index}>
              <Figure>
                <div>
                  <img
                    src={image.imageUrl || image.thumbnailUrl}
                    alt={image.altText}
                  />
                </div>
                <FigCaption id="caption">
                  <ImageCaption vocabImage={image} />
                </FigCaption>
              </Figure>
            </React.Fragment>
          );
        })}
      </ImageContainer>
    </Section>
  );
}

import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectNodesMap, actions } from '../../../../../../features/Tree';
import { VocabularyItem } from '../../../../../../model/vocabularyItems/VocabularyItem';

interface Props {
  vocab: VocabularyItem;
}

export function RelatedTermsLoader(props: React.PropsWithChildren<Props>) {
  const dispatch = useDispatch();
  const nodesMap = useSelector(selectNodesMap);

  const missingIds: string[] = useMemo(
    () =>
      props.vocab.related.hasItems
        ? props.vocab.related.items
            .filter(item => !nodesMap.hasOwnProperty(item.id))
            .map(item => item.id)
        : [],
    [nodesMap, props.vocab.related.hasItems, props.vocab.related.items],
  );

  useEffect(() => {
    if (missingIds.length) {
      dispatch(actions.loadIdsStandard({ ids: missingIds }));
    }
  }, [dispatch, missingIds]);

  return <>{missingIds.length ? null : props.children}</>;
}

import React from 'react';
import { useTranslation } from 'react-i18next';
import { VocabularyItem } from '../../../../../model/vocabularyItems/VocabularyItem';
import { AssociativeRelation } from '../../../../../model/normalizer';
import { TermWithQualifierOrEntityType } from '../../../../TermWithQualifierOrEntityType';
import { messages } from '../../messages';
import { Header, Table, TermLink, TermRelation, Section } from '../../style';

interface Props {
  vocab: VocabularyItem;
}

export function RelatedTerms(props: Props) {
  const translationTerms = useTranslation('translation');
  const translationRoles = useTranslation('roles');

  if (!props.vocab.related.hasItems && props.vocab.associativeRelations) {
    return null;
  }

  const associativeRelations: AssociativeRelation[] =
    props.vocab.associativeRelations ?? [];

  const itemsDictionary: any[] = [];

  props.vocab.related.items.forEach(item => {
    const typeOfRelation:
      | AssociativeRelation
      | undefined = associativeRelations.find(el => el.id === item.id);
    if (typeOfRelation) {
      itemsDictionary.push({
        id: item.id,
        typeOfRelation: typeOfRelation.typeOfAssociativeRelation,
        permaLink: item.permalink,
        preferredTerm: item.preferredTerm,
      });
    }
  });

  if (!itemsDictionary.length) {
    return null;
  }

  return (
    <Section aria-labelledby="header-related-terms">
      <Header id="header-related-terms">
        {translationTerms.t(messages.related_terms)}
      </Header>
      <Table>
        {itemsDictionary.map((item: any, idx: number) => (
          <React.Fragment key={`${props.vocab.id}-${idx}`}>
            <TermRelation id={`RelatedTerms-${props.vocab.id}-${idx}`}>
              {translationRoles.t(`roles.${item.typeOfRelation}`)}
            </TermRelation>
            <TermLink
              href={item.permaLink}
              aria-labelledby={`RelatedTerms-${props.vocab.id}-${idx}`}
            >
              <TermWithQualifierOrEntityType id={item.id} />
            </TermLink>
          </React.Fragment>
        ))}
      </Table>
    </Section>
  );
}

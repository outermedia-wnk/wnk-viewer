import React from 'react';
import { useTranslation } from 'react-i18next';
import { VocabularyItem } from '../../../../../model/vocabularyItems/VocabularyItem';
import {
  XTreeProcessingStatus,
  XTreeStatus,
} from '../../../../../model/normalizer';
import { messages } from '../../messages';
import { Label } from '../../style';

interface Props {
  vocab: VocabularyItem;
}

export function Status(props: Props) {
  const { t } = useTranslation();

  const status: string = props.vocab.status;
  const processingStatus: string = props.vocab.processingStatus;

  let statusKey = status;
  if (
    status === XTreeStatus.approved &&
    processingStatus === XTreeProcessingStatus.closed
  ) {
    statusKey = 'approved';
  } else if (status === XTreeStatus.candidate) {
    statusKey = 'inProgress';
  }

  return (
    <>
      <Label id="label-status">{t(messages.status)} </Label>
      <span aria-labelledby="label-status">{t(`Status.${statusKey}`)}</span>
    </>
  );
}

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Note } from '../../../../../model/vocabularyItems/Notes';
import { VocabularyItem } from '../../../../../model/vocabularyItems/VocabularyItem';
import { messages } from '../../messages';
import { Box, Header, Section } from '../../style';

interface Props {
  vocab: VocabularyItem;
}
export function UsageNotes(props: Props) {
  const { t } = useTranslation();

  if (!props.vocab.notes.hasScopeNotes) {
    return null;
  }

  return (
    <Section aria-labelledby="header-usage-notes">
      <Header id="header-usage-notes">{t(messages.usage_notes)}</Header>
      {props.vocab.notes.scopeNotes.map((note: Note, idx: number) => (
        <Box key={`${props.vocab.id}-${idx}`}>{note.value}</Box>
      ))}
    </Section>
  );
}

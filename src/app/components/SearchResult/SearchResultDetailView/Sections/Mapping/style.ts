import styled from 'styled-components/macro';
import { LI } from '../../../../HTML';
import { media } from '../../../../../../styles';
import { Label } from '../../style';

export const MappingItem = styled(LI)`
  display: -ms-grid;
  display: grid;
  -ms-grid-rows: auto;
  grid-template-rows: auto;
  -ms-grid-columns: auto ${props => props.theme.spacing._7}rem 1fr;
  grid-template-columns: auto 1fr;
  grid-column-gap: ${props => props.theme.spacing._7}rem;
  grid-template-areas: 'mapping-label' 'mapping-value';
  margin-bottom: ${props => props.theme.spacing._24}rem;

  ${media.tablet_landscape_up`
    grid-template-areas: 'mapping-label mapping-value';
    margin-bottom: ${props => props.theme.spacing._10}rem;
  `}

  ${Label} {
    grid-area: mapping-label;
  } ;
`;

export const MappingValue = styled.span`
  grid-area: mapping-value;
`;

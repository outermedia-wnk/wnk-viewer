import React from 'react';
import { useTranslation } from 'react-i18next';
import { translationOrValue } from '../../../../../../locales/utils';
import { MapItem } from '../../../../../model/vocabularyItems/MapItem';
import { VocabularyItem } from '../../../../../model/vocabularyItems/VocabularyItem';
import { ExternalLink } from '../../../../Link/ExternalLink';
import { UL } from '../../../../HTML';
import { IconInfo } from '../../../../Icon';
import { messages } from '../../messages';
import { Header, Section, Label } from '../../style';
import { MappingItem, MappingValue } from './style';

interface Props {
  vocab: VocabularyItem;
}
export function Mapping(props: Props) {
  const { t } = useTranslation();

  if (!props.vocab.mapItems || props.vocab.mapItems.length === 0) {
    return null;
  }

  return (
    <Section aria-labelledby="header-mappings">
      <Header id="header-mappings">
        {t(messages.mappings)}
        <a href="#header-mappings">
          <IconInfo title={t(messages.mappingsTooltip)} />
        </a>
      </Header>
      <UL>
        {props.vocab.mapItems.map((item: MapItem, idx: number) => {
          return (
            <MappingItem key={`${props.vocab.id}-${idx}`}>
              <Label id={`Mapping-${props.vocab.id}-${idx}`}>
                {t(`MapItem.relation.${item.mappingRelation}`)}{' '}
              </Label>
              <MappingValue>
                {['uri', 'url'].includes(item.mapItemIDType) &&
                  item.mapItemID &&
                  item.mapItemSource && (
                    <ExternalLink
                      url={item.mapItemID}
                      linkProps={{
                        title: t('Link.external.tooltip'),
                        'aria-labelledby': `Mapping-${props.vocab.id}-${idx}`,
                      }}
                      text={translationOrValue(
                        item.mapItemSource,
                        `MapItem.vocabulary.${item.mapItemSource}`,
                        t,
                      )}
                    />
                  )}
                {!item.mapItemID && item.mapItemSource && (
                  <>
                    {translationOrValue(
                      item.mapItemSource,
                      `MapItem.vocabulary.${item.mapItemSource}`,
                      t,
                    )}
                  </>
                )}
              </MappingValue>
            </MappingItem>
          );
        })}
      </UL>
    </Section>
  );
}

import styled from 'styled-components/macro';
import { UL } from '../../../../HTML';

export const List = styled(UL)`
  border-top: 1px solid ${props => props.theme.color.line_gray};
  padding-top: ${props => props.theme.spacing._16}rem;
  padding-bottom: ${props => props.theme.spacing._16}rem;

  &:last-child {
    border-bottom: 1px solid ${props => props.theme.color.line_gray};
  }
`;

import styled, { css } from 'styled-components/macro';
import { Link as ReactRouterLink } from 'react-router-dom';
import { linkInteractionStyles } from '../../../../../Link';
import { font_Body } from '../../../../../../../styles';
import { LI } from '../../../../../HTML';

interface ListItemProps {
  indent: number;
}
export const ListItem = styled(LI)<ListItemProps>`
  padding-left: ${props => Math.max(props.indent - 1, 0) * 23}px;
  svg {
    margin-left: 3px;
    margin-right: 7px;
  }
`;

export const Name = styled.span<{ $italic?: boolean }>`
  ${font_Body};
  ${props =>
    props.$italic &&
    css`
      font-style: italic;
    `};
`;
export const Link = styled(ReactRouterLink)`
  ${font_Body};
  ${linkInteractionStyles};
  text-decoration: underline;
`;

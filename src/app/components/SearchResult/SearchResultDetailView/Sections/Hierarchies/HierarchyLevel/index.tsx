import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TreeNode } from '../../../../../../api/xtree';
import {
  selectSelectedId,
  selectNodeById,
} from '../../../../../../features/Tree';
import { isStructureElement } from '../../../../../../model/isStructureElement';
import { StringUtils } from '../../../../../../model/StringUtils';
import { PermalinkBuilder } from '../../../../../../routes/PermalinkBuilder';
import { IconArrowHierarchy } from '../../../../../Icon';
import { Link, ListItem, Name } from './style';

interface Props {
  id: string;
  isPartitive: boolean;
  level: number;
}

export function HierarchyLevel(props: Props) {
  const { t } = useTranslation();
  const currentId = useSelector(selectSelectedId);
  const node: TreeNode = useSelector(selectNodeById(props.id));

  if (!node) {
    return null;
  }
  let title = StringUtils.termWithQualifier(node.preferredTerm, node.qualifier);
  title = StringUtils.termWithEntityType(title, node.entityType, t);

  const permalink = PermalinkBuilder.build(node.id);

  const isItalic = isStructureElement(node.entityType);

  return (
    <ListItem indent={props.level}>
      {props.level > 0 && (
        <IconArrowHierarchy aria-hidden={true} aria-labelledby="title" />
      )}
      {currentId === node.id ? (
        <Name $italic={isItalic}>{title}</Name>
      ) : (
        <Link to={{ pathname: permalink, search: '' }}>
          <Name $italic={isItalic}>{title}</Name>
        </Link>
      )}
      {props.isPartitive && <> {t('Hierarchy.partitive.suffix')}</>}
    </ListItem>
  );
}

import React from 'react';
import { VocabularyItem } from '../../../../../../model/vocabularyItems/VocabularyItem';
import { Header, Section } from '../../../style';
import { HierarchyLevel } from '../HierarchyLevel';
import { List } from '../style';

interface Props {
  hierarchy: string[][];
  isPartitive?: boolean;
  title: string;
  vocab: VocabularyItem;
}

export function Hierarchy(props: Props) {
  const partitiveIds = props.vocab.partitiveIds;
  return (
    <Section aria-label={`header-${props.title}`}>
      <Header id={`header-${props.title}`}>{props.title}</Header>
      <div>
        {props.hierarchy.map((path: string[], hierarchyIdx: number) => (
          <List key={hierarchyIdx}>
            {path.map((id: string, termIdx: number) => (
              <HierarchyLevel
                key={`${props.vocab.id}-${hierarchyIdx}-${id}`}
                id={id}
                level={termIdx}
                isPartitive={
                  (props.isPartitive ?? false) && partitiveIds.includes(id)
                }
              />
            ))}
          </List>
        ))}
      </div>
    </Section>
  );
}

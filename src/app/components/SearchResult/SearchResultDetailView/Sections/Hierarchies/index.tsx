import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { VocabularyItem } from '../../../../../model/vocabularyItems/VocabularyItem';
import { selectNodesMap } from '../../../../../features/Tree';
import { messages } from '../../messages';
import { Hierarchy } from './Hierarchy';

interface Props {
  vocab: VocabularyItem;
}

export function Hierarchies(props: Props) {
  const { t } = useTranslation();
  const nodesMap = useSelector(selectNodesMap);

  props.vocab.computeHierarchies(nodesMap);

  return (
    <>
      {props.vocab.hasHierarchy && (
        <Hierarchy
          vocab={props.vocab}
          hierarchy={props.vocab.hierarchy}
          title={t(messages.hierarchies)}
        />
      )}
      {props.vocab.hasPartitiveHierarchy && (
        <Hierarchy
          isPartitive
          vocab={props.vocab}
          hierarchy={props.vocab.partitiveHierarchy}
          title={t(messages.partitiveHierarchies)}
        />
      )}
    </>
  );
}

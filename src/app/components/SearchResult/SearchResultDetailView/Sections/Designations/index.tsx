import React from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { selectNormalizedData } from '../../../../../features/Search';
import { StringUtils } from '../../../../../model/StringUtils';
import { Term, Terms } from '../../../../../model/vocabularyItems/Terms';
import { VocabularyItem } from '../../../../../model/vocabularyItems/VocabularyItem';
import { messages } from '../../messages';
import {
  Designation,
  DesignationTerm,
  Header,
  Section,
  Table,
} from '../../style';

interface Props {
  vocab: VocabularyItem;
}
export function Designations(props: Props) {
  const { t } = useTranslation();
  const normalizedData = useSelector(selectNormalizedData);

  if (!props.vocab.terms || props.vocab.terms.length === 0 || !normalizedData) {
    return null;
  }
  const terms = new Terms(props.vocab.terms, normalizedData.entities.term);

  // Auflistungsreihenfolge:
  // Bevorzugte Bezeichnung (mit oder ohne Zusatz)
  // Rolle, Sprache, Numerus, Varietät (nur, wenn sie nicht Standardsprache ist).
  // Beispiel: Hängebahn, Schlagwort, Deutsch, Singular

  return (
    <Section aria-labelledby="header-designations">
      <Header id="header-designations">{t(messages.designations)}</Header>
      <Table>
        {terms.terms.map((term: Term, idx: number) => {
          const data: string[] = [];
          if (term.labelRole && props.vocab.shouldDisplayRole()) {
            data.push(t(`Term.labelRole.${term.labelRole}`));
          }
          if (term.lang) {
            data.push(t(`Language.code.${term.lang}`));
          }
          if (term.grammaticalNumber) {
            data.push(t(`Term.grammaticalNumber.${term.grammaticalNumber}`));
          }
          return (
            <React.Fragment key={`${props.vocab.id}-${idx}`}>
              <DesignationTerm
                aria-describedby={`Designation-${props.vocab.id}-${idx}`}
              >
                {StringUtils.termWithQualifier(term.Term, term.qualifier)}
              </DesignationTerm>
              <Designation id={`Designation-${props.vocab.id}-${idx}`}>
                {data.filter(d => !!d).join(', ')}
              </Designation>
            </React.Fragment>
          );
        })}
      </Table>
    </Section>
  );
}

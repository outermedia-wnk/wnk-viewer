import React from 'react';
import { useTranslation } from 'react-i18next';
import { Note } from '../../../../../model/vocabularyItems/Notes';
import { VocabularyItem } from '../../../../../model/vocabularyItems/VocabularyItem';
import { messages } from '../../messages';
import { Box, Header, Section } from '../../style';

interface Props {
  vocab: VocabularyItem;
}
export function Explanation(props: Props) {
  const { t } = useTranslation();

  if (!props.vocab.notes.hasExplanationNotes) {
    return null;
  }

  return (
    <Section aria-labelledby="header-explanation">
      <Header id="header-explanation">{t(messages.explanations)}</Header>
      {props.vocab.notes.explanations.map((note: Note, idx: number) => (
        <Box key={`${props.vocab.id}-${idx}`}>{note.value}</Box>
      ))}
    </Section>
  );
}

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Note } from '../../../../../model/vocabularyItems/Notes';
import { VocabularyItem } from '../../../../../model/vocabularyItems/VocabularyItem';
import { messages } from '../../messages';
import { Box, Header, Section } from '../../style';

interface Props {
  vocab: VocabularyItem;
}
export function Definitions(props: Props) {
  const { t } = useTranslation();

  if (!props.vocab.notes.hasDefinitions) {
    return null;
  }
  return (
    <Section aria-labelledby="header-definition">
      <Header id="header-definition">{t(messages.definition)}</Header>
      {props.vocab.notes.definitions.map((note: Note, idx: number) => (
        <Box key={`${props.vocab.id}-${idx}`}>{note.value}</Box>
      ))}
    </Section>
  );
}

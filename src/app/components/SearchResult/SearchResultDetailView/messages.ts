import { _t } from '../../../../utils/messages';

const scope = 'Page.ResultDetails';

const headersScope = `${scope}.headers`;
const headers = {
  definition: _t(`${headersScope}.definition`),
  designations: _t(`${headersScope}.designations`),
  mappings: _t(`${headersScope}.mappings`),
  mappingsTooltip: _t(`${headersScope}.mappings.tooltip`),
  explanations: _t(`${headersScope}.explanations`),
  hierarchies: _t(`${headersScope}.hierarchies`),
  partitiveHierarchies: _t(`${headersScope}.partitiveHierarchies`),
  related_terms: _t(`${headersScope}.related_terms`),
  uri: _t(`${headersScope}.uri`),
  usage_notes: _t(`${headersScope}.usage_notes`),
};

const labelsScope = `${scope}.labels`;
const labels = {
  last_update: _t(`${labelsScope}.last_update`),
  status: _t(`${labelsScope}.status`),
};

const imagesScope = `${scope}.images`;
const images = {
  photographer: _t(`${imagesScope}.photographer`),
  copyrights: _t(`${imagesScope}.copyrights`),
  license: _t(`${imagesScope}.license`),
  source: _t(`${imagesScope}.source`),
};

export const messages = {
  ...headers,
  ...labels,
  ...images,
  'cta.part1': _t(`${scope}.cta.part1`),
  'cta.part2': _t(`${scope}.cta.part2`),
};

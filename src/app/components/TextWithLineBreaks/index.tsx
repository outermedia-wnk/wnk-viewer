import React from 'react';

interface Props {
  text: string;
}

export function TextWithLineBreaks(props: Props) {
  const textParts = props.text.split('\n');
  const components: any[] = [];
  for (let i = 0; i < textParts.length; i++) {
    components.push(<span key={`text-${i}`}>{textParts[i]}</span>);
    if (i < textParts.length - 1) {
      components.push(<br key={`break-${i}`} />);
    }
  }
  return <>{components}</>;
}

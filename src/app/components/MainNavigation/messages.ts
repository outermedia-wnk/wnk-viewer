import { _t } from '../../../utils/messages';

const scope = 'Menu.MainNavigation';

export const messages = {
  about: _t(`${scope}.about`),
  browseTheViewer: _t(`${scope}.browseTheViewer`),
  team: _t(`${scope}.team`),
};

import React from 'react';
import { ContentWrapper } from '../ContentWrapper';
import { AboutUs } from './AboutUs';
import { Join } from './Join';
import { TeamContent } from './Team';
import { AboutUsSection, JoinSection, TeamGrid, TeamSection } from './style';

export function Team() {
  return (
    <TeamGrid>
      <TeamSection>
        <ContentWrapper side="left">
          <TeamContent />
        </ContentWrapper>
      </TeamSection>

      <AboutUsSection>
        <ContentWrapper side="left">
          <AboutUs />
        </ContentWrapper>
      </AboutUsSection>

      <JoinSection side="right">
        <ContentWrapper side="right">
          <Join />
        </ContentWrapper>
      </JoinSection>
    </TeamGrid>
  );
}

import React from 'react';
import teamContent from '../../../../content/team.json';
import { Title } from '../ContentWrapper';
import { PersonContact, PersonEmail, PersonName } from './style';

export function TeamContent() {
  return (
    <>
      <Title>Team</Title>

      {teamContent.people.map((item, idx) => (
        <PersonContact key={idx}>
          <PersonName>
            {item.name} {item.title}
          </PersonName>
          <div>Telefon: {item.phone}</div>
          <PersonEmail>
            E-Mail: <a href={`mailto:${item.mail}`}>{item.mail}</a>
          </PersonEmail>
        </PersonContact>
      ))}
      <br />
      <span>
        Unser Büro befindet sich in der Bachstraße 11, 53115 Bonn in
        unmittelbarer Nähe zum LVR-LandesMuseum Bonn.
      </span>
    </>
  );
}

import styled from 'styled-components/macro';
import { font_Body, font_BodyBookBold, media } from '../../../../styles';
import { LI, UL } from '../../HTML';
import {
  AboutTeaser as HomepageAboutTeaser,
  TeamTeaser as HomepageTeamTeaser,
  SearchSuggestions as HomepageSuggestionsTeaser,
  HomepageGrid,
} from '../Home/style';
import { linkInteractionStyles } from '../../Link';

export const TeamGrid = styled(HomepageGrid)`
  min-height: unset;
  grid-template-areas:
    'team'
    'about'
    'join';

  ${media.tablet_portrait_up`
    grid-template-areas:
    'team join'
    'about join';
  `};
`;
export const TeamSection = styled(HomepageAboutTeaser)`
  background-color: ${props => props.theme.color.light_blue};
  grid-area: team;
`;
export const AboutUsSection = styled(HomepageTeamTeaser)`
  background-color: ${props => props.theme.color.light_yellow};
  grid-area: about;
`;
export const JoinSection = styled(HomepageSuggestionsTeaser)`
  grid-area: join;
`;

export const PersonContact = styled.div`
  ${font_Body};
  margin-top: 0;

  img {
    max-width: 100%;
  }
`;
export const PersonName = styled.div`
  ${font_BodyBookBold}
  margin-top: ${props => props.theme.spacing._20}rem;
`;

export const PersonEmail = styled.div`
  a {
    ${font_Body};
    ${linkInteractionStyles};
  }
`;

export const List = styled(UL)``;
export const ListItem = styled(LI)`
  margin-top: ${props => props.theme.spacing._16}rem;
`;

import React from 'react';
import { TeaserTextBuch, Title } from '../ContentWrapper';

export function AboutUs() {
  return (
    <>
      <Title>Über uns</Title>
      <TeaserTextBuch>
        Die Redaktion von Wortnetz Kultur ist seit 2015 unser beider
        Hauptaufgabe im Team der Abteilung "Digitales Kulturerbe" im
        Landschaftsverband Rheinland. Dabei beschäftigen wir uns mit dem Ausbau
        und der Qualitätssicherung des kontrollierten Vokabulars. Alle
        Arbeitsschritte von der Annahme neuer Begriffe bis zur finalen Zulassung
        eines Schlagworts werden von uns sorgfältig durchgeführt und nach dem
        Vieraugenprinzip geprüft. Zeitweise werden wir von Volontär*innen oder
        studentischen Hilfskräften unterstützt. Neben der eigentlichen
        Thesaurusarbeit wirken wir auch an den Projekten zur Datenbankmigration
        von Objektdaten und zur Digitalisierung von Sammlungen in den LVR-Museen
        und wissenschaftlichen Dienststellen mit, vor allem beratend zum Thema
        der Verschlagwortung, aber auch koordinierend bei der Weiterentwicklung
        von Erfassungsmasken.
      </TeaserTextBuch>
      <TeaserTextBuch>
        Ein direkter Austausch mit unseren Nutzer*innen ist uns sehr wichtig und
        wir freuen uns über Anregungen, Fragen und auch Kritik. Melden Sie sich
        gerne bei uns!
      </TeaserTextBuch>
    </>
  );
}

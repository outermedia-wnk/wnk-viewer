import React from 'react';
import { TeaserText, TeaserTextBuch, Title } from '../ContentWrapper';
import { List, ListItem } from './style';

export function Join() {
  return (
    <>
      <Title>Mitmachen und Nutzen</Title>
      <TeaserText>
        Sie sind bereits digiCULT-Verbund-Mitglied? Sie können uns jederzeit auf
        eine Zusammenarbeit ansprechen. Auch Nicht-Mitglieder haben die
        Möglichkeit mit unserem Vokabular zu arbeiten.
        <br />
        <br />
      </TeaserText>
      <TeaserTextBuch>
        Falls eine Implementierung zur Nutzung in einem eigenen Erfassungssystem
        vorgesehen ist, bitten wir um Kontaktaufnahme zur Abstimmung technischer
        Details. Schreiben Sie uns gerne eine E-Mail, dann können wir die
        weitere Vorgehensweise besprechen.
        <br />
        <br />
      </TeaserTextBuch>

      <TeaserTextBuch>Vorab das Wichtigste in Kürze:</TeaserTextBuch>
      <List>
        <ListItem>
          Die Accounts auf eine Schnittstelle werden direkt von digiCULT
          vergeben. Den technischen Zugriff auf xTree gewährleistet die
          xTree-Rest-Schnittstelle (http://xtree-rest.digicult-verbund.de),
          welche das Vokabular in einem JSON-Format ausliefern kann.
        </ListItem>
        <ListItem>
          Kenntnisse im Umgang mit JSON
          (https://de.wikipedia.org/wiki/JavaScript_Object_Notation) und wie
          Informationen aus einem Webservice
          (https://de.wikipedia.org/wiki/Representational_State_Transfer)
          abgerufen werden, sind nötig.
        </ListItem>
      </List>
    </>
  );
}

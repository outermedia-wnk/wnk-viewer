import React from 'react';
import { OneToTwoGrid } from '../../Grid';
import { H1 } from '../../HTML';
import {
  ContentLeft,
  ContentRight,
  ContentWrapper,
  TeaserTextBuch,
  TextLink,
} from '../ContentWrapper';
import { H2, H3, BulletList } from './style';
import { NavLink } from 'react-router-dom';

export function Accessibility() {
  return (
    <OneToTwoGrid>
      <ContentLeft bgColor="yellow">
        <ContentWrapper side="left">
          <H1>Erklärung zur Barrierefreiheit</H1>

          <TeaserTextBuch>
            Der Landschaftsverband Rheinland (LVR) hat den Anspruch, seine
            Internetseiten barrierefrei zugänglich zu machen. Sie sollen so
            gestaltet sein, dass sie im Einklang mit den nationalen
            Rechtsvorschriften zur Umsetzung der Richtlinie (EU) 2016/2102 des
            Europäischen Parlaments stehen. Die folgende Erklärung zur
            Barrierefreiheit gilt für die Internetseite{' '}
            <NavLink to="/">https://wnk-viewer.lvr.de</NavLink> mit ihren
            Unterkapiteln.
          </TeaserTextBuch>

          <br />
          <H2>
            Stand der Vereinbarkeit der Internetseite mit den Anforderungen an
            barrierefreie Informationstechnik
          </H2>
          <TeaserTextBuch>
            Diese Fachanwendung erfüllt die Anforderungen an barrierefreie
            Informationstechnik noch nicht vollständig.
          </TeaserTextBuch>

          <br />
          <H3>Nicht barrierefreie Inhalte</H3>
          <TeaserTextBuch>
            Wir arbeiten derzeit daran, die Zugänglichkeit der Webseiten des
            Landschaftsverbandes Rheinland zu verbessern. Dafür bitten wir um
            etwas Geduld, da der Landschaftsverband Rheinland – mit seinen
            Schulen, Kliniken, Museen, Kultureinrichtungen, Heilpädagogischen
            Netzen, Jugendhilfeeinrichtungen und dem Landesjugendamt – eine
            große Anzahl an Webauftritten betreibt, und die
            Barrierefreiheits-Optimierungen auch Änderungen in der
            Programmierung erfordern. Wir bemühen uns um eine schnellstmögliche
            Behebung der Barrieren.
          </TeaserTextBuch>

          <br />
          <H3>Insbesondere gibt es folgende Defizite:</H3>
          <BulletList>
            <li>Kontrast des Info-Icons ist zu gering.</li>
          </BulletList>
          <br />
          <H3>Erstellung dieser Erklärung</H3>
          <TeaserTextBuch>
            Die Erklärung wurde am 21.02.2023 erstellt. Sie wurde am 23.02.2023
            zuletzt überprüft.
            <br />
            <br />
            Die Erklärung wurde auf der Grundlage eines von LVR-InfoKom
            durchgeführten BITV-Tests am 09.01.2023 verfasst.
          </TeaserTextBuch>
          <br />
        </ContentWrapper>
      </ContentLeft>

      <ContentRight>
        <ContentWrapper side="right">
          <H2>Feedback und Kontakt</H2>

          <TeaserTextBuch>
            Wenn Sie Kontakt mit dem LVR aufnehmen möchten, schreiben Sie bitte
            eine E-Mail an{' '}
            <TextLink href="mailto:wnk@lvr.de">wnk@lvr.de.</TextLink>
            <br />
            <br />
            Wir freuen uns auf Ihre Anmerkungen, Anregungen und Fragen zum Thema
            "Barrierefreiheit". Insbesondere können Sie uns etwaige Mängel
            melden und Informationen über von der EU-Richtlinie ausgenommene
            Inhalte einholen.
            <br />
            <br />
            Ansprechen können Sie dafür das{' '}
            <NavLink to="/team">Redaktionsteam</NavLink> von Wortnetz Kultur aus
            der Abteilung für Digitales Kulturerbe.
            <br />
            <br />
            Bitte beachten Sie, dass mit dem Absenden des Formulars die von
            Ihnen angegebenen personenbezogenen Daten sowie die besonderen Daten
            auf Grundlage von Artikel 6 Absatz 1 EU-DSGVO an den
            Landschaftsverband Rheinland übermittelt und zur Beantwortung der
            Anfrage verwendet werden. Lesen Sie hierzu bitte auch unsere 
            <NavLink to="/datenschutz">Datenschutzhinweise</NavLink>.
          </TeaserTextBuch>

          <br />
          <H3>
            Überwachungsstelle für barrierefreie Informationstechnik des Landes
            Nordrhein-Westfalen
          </H3>
          <TeaserTextBuch>
            Die Überwachungsstelle für barrierefreie Informationstechnik des
            Landes Nordrhein-Westfalen prüft regelmäßig, ob und inwiefern
            Webseiten und mobile Anwendungen öffentlicher Stellen des Landes den
            Anforderungen an die Barrierefreiheit genügen.
            <br />
            <br />
            Ziel der Arbeit der Überwachungsstelle ist es, die Einhaltung der
            Anforderungen an die barrierefreie Informationstechnik
            sicherzustellen und für eine flächendeckende Umsetzung der
            gesetzlichen Regelungen zu sorgen.
            <br />
            <br />
            <TextLink href="mailto:uberwachungsstelle-nrw@it.nrw.de">
              E-Mail an die Überwachungsstelle senden.
            </TextLink>
            <br />
            <br />
            <TextLink
              target="_blank"
              rel="noreferrer"
              href="https://www.mags.nrw/ueberwachungsstelle-barrierefreie-informationstechnik"
            >
              Hier finden Sie weitere Informationen zur Überwachungsstelle.
            </TextLink>
          </TeaserTextBuch>

          <br />
          <H3>
            Ombudsstelle für barrierefreie Informationstechnik des Landes
            Nordrhein-Westfalen
          </H3>
          <TeaserTextBuch>
            Sollten Sie auf Mitteilungen oder Anfragen zur barrierefreien
            Informationstechnik der Internetseite{' '}
            <NavLink to="/">https://wnk-viewer-lvr.de</NavLink>
             von der Redaktion Wortnetz Kultur keine zufriedenstellenden
            Antworten erhalten haben, können Sie die Ombudsstelle für
            barrierefreie Informationstechnik einschalten. Unter Einbeziehung
            aller Beteiligten versucht die Ombudsstelle, die Umstände der
            fehlenden Barrierefreiheit zu ermitteln, damit der Träger oder die
            Trägerin diese beheben kann.
            <br />
            <br />
            Sie ist der oder dem Beauftragten für die Belange der Menschen mit
            Behinderung nach § 11 des Behindertengleichstellungsgesetzes
            Nordrhein-Westfalen zugeordnet und über folgenden Kontakt zu
            erreichen:
            <br />
            <br />
            <TextLink href="mailto:ombudsstelle-barrierefreie-it@mags.nrw.de">
              E-Mail an die Ombudsstelle für barrierefreie Informationstechnik
              NRW senden.
            </TextLink>
            <br />
            <br />
            <TextLink
              target="_blank"
              rel="noreferrer"
              href="https://www.mags.nrw/ombudsstelle-barrierefreie-informationstechnik"
            >
              Hier finden Sie weitere Informationen zur Ombudsstelle für
              barrierefreie Informationstechnik NRW.
            </TextLink>
          </TeaserTextBuch>
        </ContentWrapper>
      </ContentRight>
    </OneToTwoGrid>
  );
}

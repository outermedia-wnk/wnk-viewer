import React from 'react';
import { H1 } from '../../HTML';
import { OneToTwoGrid } from '../../Grid';
import {
  Address,
  ContentLeft,
  ContentRight,
  ContentWrapper,
  Department,
  TeaserTextBuch,
  TeaserTitle,
  TextLink,
} from '../ContentWrapper';
import { H3Default } from '../Impressum/style';

export function Datenschutz() {
  return (
    <OneToTwoGrid>
      <ContentLeft bgColor="yellow">
        <ContentWrapper side="left">
          <H1>Datenschutzerklärung</H1>
          <TeaserTextBuch>
            Die Einhaltung des Datenschutzes hat für den LVR höchste Priorität.
            Deshalb ist die Beachtung der datenschutzrechtlichen Bestimmungen
            für uns selbstverständlich. Wenn Sie das Internetangebot des LVR
            nutzen, verarbeitet der LVR Ihre Daten nur gemäß den entsprechenden
            Vorgaben der Aufsichtsbehörden und im Rahmen der gesetzlichen
            Bestimmungen. Die maßgeblichen Vorschriften dazu enthalten das
            Telemediengesetz und das Datenschutzgesetz NRW sowie die Europäische
            Datenschutzgrundverordnung (EU-DSGVO).
          </TeaserTextBuch>

          <TeaserTitle>
            Aufzeichnung zum Zweck der Sicherstellung der Systemintegrität
          </TeaserTitle>
          <TeaserTextBuch>
            Alle Zugriffe auf diese Webseite werden registriert. Es werden
            Datum, Rechneradresse sowie besuchte Seiten gespeichert. Diese Daten
            werden für statistische Zwecke ausgewertet und ggf. anonymisiert
            veröffentlicht. Zur Sicherstellung der Systemintegrität sowie zum
            Schutz gegen unbefugte und rechtswidrige Nutzung der
            Internet-Angebote des LVR wird die bei Ihrem jeweiligen Seitenaufruf
            erzeugte IP-Adresse sieben Tage gespeichert und sodann automatisch
            gelöscht.
          </TeaserTextBuch>
          <TeaserTextBuch>
            Mit der Benutzung dieser Webseite erklären Sie sich hiermit
            einverstanden.
          </TeaserTextBuch>

          <br />
          <H3Default>Verantwortlich für die Datenverarbeitung</H3Default>
          <Department>LVR-Direktorin Ulrike Lubek</Department>
          <br />
          <Address>
            Landschaftsverband Rheinland
            <br />
            Kennedy-Ufer 2<br />
            50679 Köln
            <br />
            Tel: 0221 809-0
            <br />
            Fax: 0221 809-2200
            <br />
            E-Mail: <TextLink href="mailto:post@lvr.de">post@lvr.de</TextLink>
          </Address>
          <br />
          <Address>
            Dezernentin Dr. Corinna Franz
            <br />
            Tel: +49 (0) 221 / 809 - 37 85 oder 37 86
            <br />
            Fax: +49 (0) 221 / 809 - 36 79
            <br />
            E-Mail:{' '}
            <TextLink href="mailto:kultur@lvr.de">kultur@lvr.de</TextLink>
          </Address>
        </ContentWrapper>
      </ContentLeft>

      <ContentRight>
        <ContentWrapper side="right">
          <TeaserTitle>Umgang mit personenbezogenen Daten</TeaserTitle>
          <TeaserTextBuch>
            Wenn Sie die Internetseiten des LVR besuchen, verarbeitet der LVR
            Ihre personenbezogenen Daten im Rahmen der gesetzlichen
            Bestimmungen. Ihre Daten werden nur so lange verarbeitet (erhoben,
            gespeichert und verwendet), wie dies für die Erbringung der auf den
            Internetseiten des LVR angebotenen Leistungen erforderlich ist.
            Danach werden die Daten unverzüglich gelöscht. Wenn Sie das
            Internetangebot des LVR nutzen, verarbeitet der LVR Ihre Daten nur
            gemäß den entsprechenden Vorgaben der Aufsichtsbehörden und im
            Rahmen der gesetzlichen Bestimmungen. Die maßgeblichen Vorschriften
            dazu enthalten das Telemediengesetz und das Datenschutzgesetz NRW.
          </TeaserTextBuch>
          <TeaserTextBuch>
            Bei einzelnen eGovernment-Angeboten auf diesen Seiten beachten Sie
            bitte dort die hinterlegten besonderen Hinweise zum Datenschutz.
          </TeaserTextBuch>
          <TeaserTextBuch>
            Sie sollten ferner beachten, dass bei einer unverschlüsselten
            Übermittlung von Daten im Internet die Möglichkeit besteht, dass
            Dritte diese zur Kenntnis nehmen oder verfälschen. Vertrauliche
            Daten sollten Sie nicht mittels unverschlüsselter E-Mail übersenden.
          </TeaserTextBuch>
          <TeaserTextBuch>
            Sie können jederzeit Auskunft über die über Sie gespeicherten
            personenbezogenen Daten erhalten oder diese einsehen. Sind Ihre
            Daten unrichtig, so können Sie deren Berichtigung verlangen. Sind
            Sie der Auffassung, dass Ihre Daten unrichtig sind, so können Sie
            deren Berichtigung verlangen.
          </TeaserTextBuch>
          <Address>
            Dezernentin Dr. Corinna Franz
            <br />
            Tel: +49 (0) 221 / 809 - 37 85 oder 37 86
            <br />
            Fax: +49 (0) 221 / 809 - 36 79
            <br />
            E-Mail:{' '}
            <TextLink href="mailto:kultur@lvr.de">kultur@lvr.de</TextLink>
          </Address>

          <br />
          <TeaserTextBuch $noMarginBottom>
            Sind Sie der Auffassung, dass Ihre Daten nicht ordnungsgemäß
            verarbeitet werden, ist Ihnen der LVR-Datenschutzbeauftragte gern
            behilflich:
          </TeaserTextBuch>
          <Address>
            50663 Köln
            <br />
            0221-809 2550
            <br />
            <TextLink href="mailto:datenschutzbeauftragter@lvr.de">
              datenschutzbeauftragter@lvr.de
            </TextLink>
            <br />
            <br />
          </Address>
          <TeaserTextBuch>
            Darüber hinaus können Sie sich in diesem Fall auch an die
            Landesbeauftragte für Datenschutz und Informationsfreiheit NRW,
            Kavalleriestr. 2-4, 40213 Düsseldorf, poststelle@ldi.nrw.de wenden.
          </TeaserTextBuch>
        </ContentWrapper>
      </ContentRight>
    </OneToTwoGrid>
  );
}

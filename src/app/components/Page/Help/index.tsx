import React from 'react';
import { ContentWrapper } from '../ContentWrapper';
import { H1, P } from '../../HTML';
import { HelpPageGrid, LeftContent, RightContent } from './style';

export function Help() {
  return (
    <HelpPageGrid>
      <LeftContent>
        <ContentWrapper side="left">
          <H1>Help</H1>
        </ContentWrapper>
      </LeftContent>

      <RightContent>
        <ContentWrapper side="right">
          <P>...</P>
        </ContentWrapper>
      </RightContent>
    </HelpPageGrid>
  );
}

import styled from 'styled-components/macro';
import { OneToTwoGrid } from '../../Grid';
import { pageContentPadding } from '../../PageLayout/style';

export const HelpPageGrid = styled(OneToTwoGrid)``;

export const LeftContent = styled.div`
  grid-area: left;
  ${pageContentPadding};
  background-color: ${props => props.theme.color.light_yellow};
  padding-top: ${props => props.theme.spacing._80}rem;
`;

export const RightContent = styled.div`
  grid-area: right;
  ${pageContentPadding};
  padding-top: ${props => props.theme.spacing._80}rem;
`;

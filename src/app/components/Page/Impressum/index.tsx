import React from 'react';
import { OneToTwoGrid } from '../../Grid';
import { H1 } from '../../HTML';
import {
  ContentLeft,
  ContentRight,
  ContentWrapper,
  TeaserTextBuch,
  TextLink,
  Address,
  Department,
} from '../ContentWrapper';
import { Routes } from '../../../routes';
import { H2, H3, BulletList, H3Default } from './style';

export function Impressum() {
  return (
    <OneToTwoGrid>
      <ContentLeft bgColor="yellow">
        <ContentWrapper side="left">
          <H1>Impressum</H1>

          <TeaserTextBuch>
            Dieses Internetangebot wird herausgegeben vom Landschaftsverband
            Rheinland (LVR). Der LVR ist eine Körperschaft des öffentlichen
            Rechts. Er wird vertreten durch die LVR-Direktorin Ulrike Lubek.
          </TeaserTextBuch>

          <br />
          <H2>Kontakt</H2>
          <Address>
            Wortnetz Kultur (WNK)
            <br />
            LVR-Dezernat Kultur und Landschaftliche Kulturpflege
            <br />
            Abt. Digitales Kulturerbe LVR
            <br />
            Bachstraße 11
            <br />
            53115 Bonn
            <br />
            Tel: 0228 2070-276
            <br />
            Tel: 0228 2070-269
            <br />
            E-Mail: <TextLink href="mailto:wnk@lvr.de">wnk@lvr.de</TextLink>
          </Address>
          <br />
          <Address>
            Landschaftsverband Rheinland
            <br />
            Kennedy-Ufer 2<br />
            50679 Köln
            <br />
            Tel: 0221 809-0
            <br />
            Fax: 0221 809-2200
            <br />
            E-Mail: <TextLink href="mailto:post@lvr.de">post@lvr.de</TextLink>
          </Address>

          <br />
          <H3Default>Zuständige Aufsichtsbehörde:</H3Default>
          <Address>
            Ministerium für Heimat, Kommunales, Bau und Gleichstellung des
            Landes Nordrhein-Westfalen
            <br />
            Jürgensplatz 1<br />
            40213 Düsseldorf
          </Address>

          <br />
          <H3Default>
            Verantwortlich im Sinne des § 18 Medienstaatsvertrag (MStV):
          </H3Default>
          <Address>
            LVR-Fachbereich Kommunikation
            <br />
            Leitung: Christine Bayer
            <br />
            Tel: 0221 809-2781
            <br />
            Fax: 0221 809-2889
          </Address>

          <br />
          <H3Default>
            Verantwortlich im LVR-Dezernat Kultur und Umwelt:
          </H3Default>
          <Address>
            Dezernentin Dr. Corinna Franz
            <br />
            Tel: +49 (0) 221 / 809 - 37 85 oder 37 86
            <br />
            Fax: +49 (0) 221 / 809 - 36 79
            <br />
            E-Mail:{' '}
            <TextLink href="mailto:kultur@lvr.de">kultur@lvr.de</TextLink>
          </Address>

          <br />
          <H3Default>
            Verantwortlich in der Abteilung Digitales Kulturerbe:
          </H3Default>
          <Department>Redaktion</Department>
          <Address $doubleAddress>
            Andrea Granderath M.A.
            <br />
            Tel: 0228 2070-276
            <br />
            E-Mail:{' '}
            <TextLink href="mailto:andrea.granderath@lvr.de">
              andrea.granderath@lvr.de
            </TextLink>
          </Address>
          <Address $doubleAddress>
            Andrea Schenk M.A.
            <br />
            Tel: 0228 2070-269
            <br />
            E-Mail:{' '}
            <TextLink href="mailto:andrea.schenk@lvr.de">
              andrea.schenk@lvr.de
            </TextLink>
          </Address>
        </ContentWrapper>
      </ContentLeft>

      <ContentRight>
        <ContentWrapper side="right">
          <H2>Gesamtkoordination des Internet-Angebotes:</H2>
          <TeaserTextBuch>
            LVR-Fachbereich Kommunikation Online-Redaktion
            <br />
            Jürgen Grommes / Andrea Steinert
            <br />
            <TextLink href="mailto:onlineredaktion@lvr.de">
              onlineredaktion@lvr.de
            </TextLink>
          </TeaserTextBuch>

          <br />
          <H3>Datenbank und Datenbereitstellung</H3>
          <TeaserTextBuch>
            <TextLink
              href="https://www.digicult-verbund.de"
              target="_blank"
              rel="noreferrer"
            >
              digiCULT Verbund, Kiel
            </TextLink>
          </TeaserTextBuch>

          <br />
          <H3>Technik und Gestaltung Frontend</H3>
          <TeaserTextBuch>
            <TextLink
              href="https://www.outermedia.de"
              target="_blank"
              rel="noreferrer"
            >
              OUTERMEDIA GmbH, Berlin
            </TextLink>
          </TeaserTextBuch>

          <br />
          <H3>Web-Server und technische Betreuung</H3>
          <TeaserTextBuch>
            <TextLink
              href="https://infokom.lvr.de/de/startpage.html"
              target="_blank"
              rel="noreferrer"
            >
              LVR InfoKom
            </TextLink>
          </TeaserTextBuch>

          <br />
          <TeaserTextBuch>
            Haben Sie technische Fragen zur Funktion der Website?
            <br />
            Dann schicken Sie doch einfach eine Mail an:{' '}
            <TextLink href="mailto:webmaster@lvr.de">webmaster@lvr.de</TextLink>
          </TeaserTextBuch>

          <br />
          <br />
          <H2>Rechtliche Hinweise</H2>
          <H3>Datenschutz</H3>
          <TeaserTextBuch>
            <TextLink href={Routes.Datenschutz}>
              Informationen zum Thema Datenschutz erhalten Sie hier.
            </TextLink>
          </TeaserTextBuch>

          <br />
          <H3>Haftungshinweise</H3>
          <TeaserTextBuch>
            Die Informationen des Online-Angebotes werden ständig aktualisiert.
            Trotz sorgfältiger Bearbeitung können sich Daten verändert haben
            oder Fehler aufgetreten sein. Hierfür bitten wir um Verständnis.
            Bitte benachrichtigen Sie uns, wenn unsere eigenen Inhalte eventuell
            nicht völlig fehlerfrei, aktuell und vollständig sind.
          </TeaserTextBuch>

          <br />
          <H3>1. Haftung im Sinne des § 7 TMG</H3>
          <TeaserTextBuch>
            Der Landschaftsverband Rheinland (LVR) stellt sein Internetangebot
            unter folgenden Nutzungsbedingungen zur Verfügung:
            <BulletList>
              <li>
                Der LVR ist als Diensteanbieter nach § 7 Abs. 1 TMG für die
                eigenen Inhalte, die er zur Nutzung bereithält, nach den
                allgemeinen Vorschriften verantwortlich. Die Haftung für Schäden
                materieller oder ideeller Art, die durch die Nutzung der Inhalte
                verursacht wurden, ist ausgeschlossen, sofern nicht Vorsatz oder
                grobe Fahrlässigkeit vorliegt.
              </li>
              <li>
                Soweit ein Text von dritter Seite erstellt ist, wird der
                jeweilige Verfasser namentlich benannt. In diesen Fällen ist der
                Verfasser des jeweiligen Dokuments bzw. sein Auftraggeber für
                den Inhalt verantwortlich.
              </li>
            </BulletList>
          </TeaserTextBuch>

          <br />
          <H3>2. Haftungsausschluss im Sinne § 8 TMG</H3>
          <TeaserTextBuch>
            Der LVR macht sich den Inhalt der per Hyperlinks zugänglich
            gemachten fremden Websites ausdrücklich nicht zu eigen und kann
            deshalb für deren inhaltliche Korrektheit, Vollständigkeit und
            Verfügbarkeit keine Gewähr leisten. Der LVR hat keinen Einfluss auf
            die aktuelle und zukünftige Gestaltung und auf Inhalte der gelinkten
            Seiten und distanziert sich ausdrücklich hiervon. Sofern der LVR
            feststellt oder davon Kenntnis erhält, dass sich auf den verlinkten
            Websites zwischenzeitlich rechtswidrige oder mit dem Leitbild des
            LVR unvereinbare Inhalte befinden, werden die entsprechenden Links
            unverzüglich entfernt.
          </TeaserTextBuch>

          <br />
          <H3>Urheberrecht</H3>
          <TeaserTextBuch>
            Die Inhalte der Internetseiten des LVR sind urheberrechtlich
            geschützt. Dies gilt insbesondere für Fotos bzw. Grafiken und Logos,
            Videodateien sowie für alle Texte und Daten. Das im WNK-Viewer
            enthaltene Vokabular steht unter der Creative Commons-Lizenz
            Namensnennung 4.0 International (CC BY 4.0), deren Bedingungen Sie{' '}
            <TextLink
              href="https://creativecommons.org/licenses/by/4.0/deed.de"
              target="_blank"
              rel="noreferrer"
            >
              hier
            </TextLink>{' '}
            nachlesen können. Eine Verwendung der im WNK-Viewer gezeigten Bilder
            ist gemäß den Bestimmungen des in den Metadaten aufgeführten
            Lizenztyps erlaubt.
          </TeaserTextBuch>
        </ContentWrapper>
      </ContentRight>
    </OneToTwoGrid>
  );
}

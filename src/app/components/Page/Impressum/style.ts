import styled from 'styled-components/macro';
import { H2 as BaseH2, H3 as BaseH3 } from '../../HTML';

export const H2 = styled(BaseH2)`
  margin-bottom: ${props => props.theme.spacing._18}rem;
`;

export const H3 = styled(BaseH3)`
  margin-bottom: ${props => props.theme.spacing._15}rem;
`;
export const H3Default = BaseH3;

export const BulletList = styled.ul`
  margin: ${props => props.theme.spacing._10}rem 0 0;
  padding: 0 ${props => props.theme.spacing._20}rem 0;
`;

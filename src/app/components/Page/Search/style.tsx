import styled, { css } from 'styled-components/macro';
import { media } from '../../../../styles';
import { ContentViewType } from '../../../features/ContentViewSwitcher';
import { pageContentPadding } from '../../PageLayout/style';

const treeSideStyles = css`
  background-color: ${props => props.theme.color.white};
  color: ${props => props.theme.color.black};

  ${media.tablet_portrait_up`
    background-color: ${props => props.theme.color.blue_gray};
  `};
`;

const resultSideStyles = css<ResultViewProps>`
  background-color: ${props => props.theme.color.white};
  color: ${props => props.theme.color.black};

  ${media.tablet_portrait_up`
    background-color: ${props =>
      props.isSearchPage
        ? props.theme.color.dark_blue_80_percent
        : props.theme.color.white};
  `}
`;

export const SearchPageGrid = styled.div`
  min-height: 100%;

  display: -ms-grid;
  display: grid;
  grid-column-gap: 0;

  grid-template-rows: auto 1fr;
  -ms-grid-rows: auto 1fr;

  grid-template-columns: 1fr;
  -ms-grid-columns: 1fr;

  grid-template-areas:
    'searchPage--view-switcher'
    'searchPage--content';

  ${media.tablet_portrait_up`
    grid-template-rows: auto;
    -ms-grid-rows: auto;

    grid-template-columns: 1fr 1fr;
    -ms-grid-columns: 1fr ${props => props.theme.grid.gutter.mobile}rem 1fr;

    grid-template-areas:
      'searchPage--view-tree searchPage--view-result';
  `};

  ${media.mobile`
    ${treeSideStyles}
  `};
`;

export const TabSwitcherWrapper = styled.div`
  background-color: ${props => props.theme.color.blue_gray};
  grid-area: searchPage--view-switcher;

  ${pageContentPadding};
  padding-top: ${props => props.theme.grid.gutter.mobile}rem;

  ${media.tablet_portrait_up`
    display: none;
  `};
`;

interface ViewProps {
  readonly currentView: ContentViewType;
}
const Tab = styled.div<ViewProps>`
  grid-area: searchPage--content;
  ${pageContentPadding};
  padding-top: ${props => props.theme.spacing._40}rem;
  padding-bottom: ${props => props.theme.spacing._40}rem;
`;

interface TreeViewProps extends ViewProps {
  maxHeight: number;
}
export const TreeView = styled(Tab)<TreeViewProps>`
  ${props =>
    props.currentView !== ContentViewType.Tree &&
    media.mobile`
      display: none;
    `};

  ${media.tablet_portrait_up`
    grid-area: searchPage--view-tree;
    ${treeSideStyles};
    overflow-y: auto;
    max-height: 100%;
  `};
`;

interface ResultViewProps extends ViewProps {
  isSearchPage: boolean;
}
export const ResultView = styled(Tab)<ResultViewProps>`
  ${resultSideStyles};
  ${props =>
    props.currentView === ContentViewType.Tree &&
    media.mobile`
      display: none;
    `};

  ${media.tablet_portrait_up`
    grid-area: searchPage--view-result;
  `};
`;

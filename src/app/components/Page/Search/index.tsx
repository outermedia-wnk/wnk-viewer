import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import {
  ContentViewType,
  selectCurrentView,
  useContentViewSwitcherSlice,
} from '../../../features/ContentViewSwitcher';
import { useSearchSlice } from '../../../features/Search';
import { useQueryParam } from '../../../hooks/useQuery';
import { RouteUtils } from '../../../routes';
import { ContentViewSwitchButton } from '../../Button/ContentViewSwitchButton';
import { HiddenH1 } from '../../HTML';
import { SearchResultDetailView } from '../../SearchResult/SearchResultDetailView';
import { SearchResultListView } from '../../SearchResult/SearchResultListView';
import { Tree } from '../../Tree';
import { ContentWrapper } from '../ContentWrapper';
import {
  ResultView,
  SearchPageGrid,
  TabSwitcherWrapper,
  TreeView,
} from './style';

export function Search() {
  useContentViewSwitcherSlice();
  const { actions } = useSearchSlice();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const location = useLocation();
  const isSearchPage = RouteUtils.isSearchPage(location);
  const { id } = useParams();
  const term: string | null = useQueryParam('term');

  useEffect(() => {
    if (term || id) {
      dispatch(actions.search({ term: term || undefined, id }));
    }
  }, [dispatch, term, id, actions]);

  const currentView = useSelector(selectCurrentView);

  const [resultViewHeight, setResultViewHeight] = useState(0);
  const resultViewRef = useCallback(node => {
    if (node !== null) {
      setResultViewHeight(node.getBoundingClientRect().height);
    }
  }, []);

  return (
    <SearchPageGrid>
      <TabSwitcherWrapper>
        <ContentViewSwitchButton
          view={ContentViewType.Tree}
          aria-expanded={currentView === ContentViewType.Tree}
          aria-controls={
            currentView === ContentViewType.Tree
              ? 'tab-content-tree'
              : 'tab-content-result'
          }
        />
        {(!isSearchPage || !term) && (
          <ContentViewSwitchButton view={ContentViewType.Details} />
        )}
        {isSearchPage && !!term && (
          <ContentViewSwitchButton view={ContentViewType.SearchResults} />
        )}
      </TabSwitcherWrapper>

      <TreeView
        id="tab-content-tree"
        currentView={currentView}
        maxHeight={resultViewHeight}
      >
        <ContentWrapper side="left">
          <HiddenH1
            id="header-tree"
            aria-label={t('Page.Search.Tree.H1.ariaLabel')}
          />
          <Tree />
        </ContentWrapper>
      </TreeView>

      <ResultView
        id="tab-content-result"
        ref={resultViewRef}
        currentView={currentView}
        isSearchPage={isSearchPage}
      >
        <ContentWrapper side="right">
          {isSearchPage && !!term && <SearchResultListView />}
          {isSearchPage && !term && <div />}
          {!isSearchPage && <SearchResultDetailView />}
        </ContentWrapper>
      </ResultView>
    </SearchPageGrid>
  );
}

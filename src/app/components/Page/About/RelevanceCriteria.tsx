import React from 'react';
import {
  Criteria,
  CriteriaLabel,
  CriteriaValue,
  TeaserTextBuch,
  TitleRightTeaser2,
} from './style';

export function RelevanceCriteria() {
  return (
    <>
      <TitleRightTeaser2>
        Welche Relevanzkritierien für Schlagworte gibt es?
      </TitleRightTeaser2>
      <TeaserTextBuch>
        Das kontrollierte Vokabular ist standardkonform aufgebaut.
        Vorgeschlagene neue Bezeichnungen müssen bestimmte Eigenschaften
        erfüllen, damit sie in den Thesaurus integriert werden können. Ein
        Begriff wird als Schlagwort in Wortnetz Kultur aufgenommen, wenn…
      </TeaserTextBuch>

      <Criteria>
        <CriteriaLabel>Nachweisbarkeit</CriteriaLabel>
        <CriteriaValue>
          … er in einschlägiger Literatur oder anderen verlässlichen Quellen
          verifizierbar ist.
        </CriteriaValue>
      </Criteria>
      <Criteria>
        <CriteriaLabel>Gebräuchlichkeit</CriteriaLabel>
        <CriteriaValue>
          … er dem allgemeinen Sprachgebrauch der Standardsprache oder der
          Fachsprache entspricht. Veraltete Bezeichnungen können einen
          historischen Begriff repräsentieren, der bei Bedarf als eigenes
          Schlagwort ausgewiesen wird. Diese Begriffe werden dann in der Regel
          über eine Assoziationsbeziehung miteinander verbunden.
        </CriteriaValue>
      </Criteria>
      <Criteria>
        <CriteriaLabel>Eindeutigkeit</CriteriaLabel>
        <CriteriaValue>
          … er eine eindeutige Bedeutung hat. Eine mehrdeutige Bezeichnung
          erhält einen identifizierenden Zusatz zur Bedeutungsklärung.
        </CriteriaValue>
      </Criteria>
    </>
  );
}

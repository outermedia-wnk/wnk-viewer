import React from 'react';
import { TeaserText, TeaserTextBuch, TeaserTitle } from './style';

export function WhatIsWNK() {
  return (
    <>
      <TeaserTitle>Was ist das Wortnetz Kultur?</TeaserTitle>
      <TeaserText>
        Wortnetz Kultur ist ein professionelles Werkzeug zur Verschlagwortung
        von Museumsobjekten, Denkmälern, Fotografien, Textdokumenten sowie
        Multimedia-Dateien und unterstützt Nutzer*innen, Wissen für den späteren
        Datenabruf zu organisieren.
        <br />
      </TeaserText>
      <TeaserTextBuch>
        Der Thesaurus wurde entwickelt, um Wortlisten und Terminologien in den
        Kulturdienststellen des Landschaftsverbands Rheinland zu
        vereinheitlichen und den Datenaustausch zu ermöglichen.
      </TeaserTextBuch>
      <TeaserTextBuch>
        Wortnetz Kultur versteht sich nicht als ein fertiges Produkt, sondern
        wird stetig erweitert und qualitätsgesichert. Wir arbeiten sehr
        bedarfsorientiert, das heißt, dass wir in einem engen Austausch mit
        unseren internen und externen Korrespondent*innen stehen und uns
        gemeinsam Fragen zur Indexierung oder der Suche nach passenden Begriffen
        für digitale Kulturobjekte widmen.
      </TeaserTextBuch>
      <TeaserTextBuch>
        Durch eine integrierte Mediendatenbank besteht die Option, die Begriffe
        mit Bilddateien zur Illustration der Begriffsbedeutung zu ergänzen.
        Gerne nutzen wir Bildmaterial aus den hauseigenen Beständen unserer
        Partner. Auf diese Weise ist ein kleiner Einblick in museale Sammlungen
        möglich.
      </TeaserTextBuch>
      <TeaserTextBuch>
        Das Vokabular steht interessierten Nutzer*innen über den WNK-Viewer als
        Linked Open Data zur Verfügung. Jeder Begriff ist durch einen
        universellen Identifikator (URI) eindeutig adressierbar.
      </TeaserTextBuch>
    </>
  );
}

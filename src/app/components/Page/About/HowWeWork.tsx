import React from 'react';
import { TeaserTextBuch, TextLink, TitleRightTeaser1 } from './style';

export function HowWeWork() {
  return (
    <>
      <TitleRightTeaser1>Wie arbeiten wir?</TitleRightTeaser1>
      <TeaserTextBuch>
        Wir erfassen und qualifizieren seit 2009 Begriffe in unserem Thesaurus,
        der Schlagworte aus unterschiedlichen Themenbereichen enthält.
        Inhaltliche Schwerpunkte bilden Begriffe aus kulturanthropologischen,
        archäologischen, kunstgeschichtlichen und kulturlandschaftlichen
        Bereichen. Verwaltet werden die Begriffe von uns in der webbasierten
        Datenbank digiCULT.xTree der digiCULT-Verbund eG, die wir als
        Genossenschaftsmitglied nutzen. Das Vokabular wird von uns permanent
        aktualisiert. Die Inhalte werden umgehend im WNK Viewer veröffentlicht.
      </TeaserTextBuch>
      <TeaserTextBuch>
        Momentan stehen rund 10.000 Begriffe zur inhaltlichen Erschließung für
        unterschiedliche Fachdatenbanken und Portale zur Verfügung. Das Wortnetz
        Kultur entsteht nach dem Vorbild des Art and Architecture Thesaurus des
        Getty Research Institute (AAT) und gliedert sich in Facetten,
        Hierarchien und Begriffe, die miteinander in Beziehung gesetzt werden
        können. Über diese logisch-hierarchische Einordnung hinaus, können
        Nutzer*innen zusätzlich eine anwendungsbezogene thematische Systematik
        erstellen und pflegen, die als Basis für eine facettierte Navigation in
        einem Portal genutzt werden kann. Ein Anwendungsbeispiel bietet{' '}
        <TextLink
          href="https://alltagskulturen.lvr.de/de/start"
          target="_blank"
          rel="noreferrer"
        >
          das Portal Alltagskulturen im Rheinland
        </TextLink>{' '}
        mit{' '}
        <TextLink
          href="http://xtree-public.digicult-verbund.de/vocnet/?uriVocItem=http://lvr.vocnet.org/wnk/&startNode=wk017784&lang=de&d=n"
          target="_blank"
          rel="noreferrer"
        >
          dazugehöriger Systematik.
        </TextLink>
      </TeaserTextBuch>
      <TeaserTextBuch>
        Jedes Schlagwort erhält eine von uns verfasste Definition, die die
        intendierte Bedeutung des Begriffs kurz und prägnant beschreibt. Wir
        erfassen Synonyme und übersetzten die bevorzugte Bezeichnung ins
        Englische. Mehrdeutige Bezeichnungen werden aufgeschlüsselt und durch
        einen Homonymzusatz ergänzt. Die Bezeichnung wird dadurch eindeutig und
        repräsentiert exakt einen Begriff.
      </TeaserTextBuch>
      <TeaserTextBuch>
        Uns ist eine Vernetzung der Begriffe mit denen anderer Thesauri oder
        Klassifikationen wichtig. Aus diesem Grund verweisen wir durch ein
        Mapping auf weitere standardkonforme Normvokabulare. Diese Relationen
        zwischen bedeutungsähnlichen oder übereinstimmenden Begriffen
        unterstützt die Interoperabilität und gewährleistet die Gebräuchlichkeit
        eines Schlagwortes. Veraltete oder historische Bezeichnungen werden
        gesondert gekennzeichnet oder bei Bedarf als eigenes Schlagwort
        aufgenommen.
      </TeaserTextBuch>
    </>
  );
}

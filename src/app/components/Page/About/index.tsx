import React from 'react';
import { OneToTwoGrid } from '../../Grid';
import { ContentLeft, ContentRight, ContentWrapper } from '../ContentWrapper';
import { PageTitle } from './style';
import { WhatIsWNK } from './WhatIsWNK';
import { HowWeWork } from './HowWeWork';
import { RelevanceCriteria } from './RelevanceCriteria';

export function About() {
  return (
    <OneToTwoGrid>
      <ContentLeft bgColor="green">
        <ContentWrapper side="left">
          <PageTitle>Mehr Informationen</PageTitle>
          <WhatIsWNK />
        </ContentWrapper>
      </ContentLeft>

      <ContentRight>
        <ContentWrapper side="right">
          <HowWeWork />
          <RelevanceCriteria />
        </ContentWrapper>
      </ContentRight>
    </OneToTwoGrid>
  );
}

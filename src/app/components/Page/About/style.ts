import styled from 'styled-components/macro';
import { font_BodyBold, font_BodyLight, media } from '../../../../styles';
import { H1, H2 } from '../../HTML';
import { Grid } from '../../Grid';

export {
  TeaserText,
  TeaserTextLight,
  TeaserTextBuch,
  TextLink,
} from '../ContentWrapper';

export const PageTitle = styled(H1)`
  margin-bottom: ${props => props.theme.spacing._44}rem;
  ${media.tablet_portrait_up`
    margin-bottom: ${props => props.theme.spacing._52}rem;
  `};
`;

export const TeaserTitle = styled(H2)`
  margin-bottom: ${props => props.theme.spacing._16}rem;
  ${media.tablet_portrait_up`
    margin-bottom: ${props => props.theme.spacing._18}rem;
  `};
`;

export const TitleRightTeaser1 = styled(TeaserTitle)`
  ${media.desktop_up`
    margin-top: ${props => props.theme.spacing._92}rem;
  `};
`;

export const TitleRightTeaser2 = styled(TeaserTitle)`
  margin-top: ${props => props.theme.spacing._72}rem;
  ${media.desktop_up`
    margin-top: ${props => props.theme.spacing._92}rem;
  `};
`;

export const Criteria = styled(Grid)`
  margin-bottom: ${props => props.theme.spacing._30}rem;
  grid-template-columns: 1fr;
  -ms-grid-columns: 1fr;

  grid-template-areas:
    'left'
    'right';

  ${media.tablet_portrait_up`
    margin-bottom: ${props => props.theme.spacing._30}rem;
    grid-column-gap: ${props => props.theme.spacing._20}rem;
    grid-template-columns: 1fr 4fr;
    -ms-grid-columns: 1fr ${props => props.theme.spacing._20}rem 3fr;

    grid-template-areas:
      'left right';
  `};

  ${media.desktop_up`
    grid-column-gap: ${props => props.theme.spacing._35}rem;
    grid-template-columns: 1fr 3fr;
    -ms-grid-columns: 1fr ${props => props.theme.spacing._35}rem 3fr;
  `};
`;

export const CriteriaLabel = styled.div`
  ${font_BodyBold};
  grid-area: left;
`;

export const CriteriaValue = styled.div`
  ${font_BodyLight};
  grid-area: right;
`;

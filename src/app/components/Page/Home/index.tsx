import React from 'react';
import { useTranslation } from 'react-i18next';
import suggestedTerms from '../../../../content/homepage-suggested-terms.json';
import { Routes } from '../../../routes';
import { RoundedLinkButton } from '../../Button/RoundedButton';
import { TextWithLineBreaks } from '../../TextWithLineBreaks';
import { ContentWrapper } from '../ContentWrapper';
import { messages } from './messages';
import {
  AboutTeaser,
  H2,
  HomepageGrid,
  Paragraph,
  TeaserList,
  TeaserListItem,
  TeaserLink,
  TeamTeaser,
  SearchSuggestions,
} from './style';

export function Home() {
  const { t } = useTranslation();

  return (
    <HomepageGrid>
      <AboutTeaser>
        <ContentWrapper side="left">
          <H2>{t(messages['AboutTeaser.title'])}</H2>
          <Paragraph>{t(messages['AboutTeaser.content'])}</Paragraph>
          <RoundedLinkButton to={{ pathname: Routes.About }}>
            {t(messages['AboutTeaser.linkText'])}
          </RoundedLinkButton>
        </ContentWrapper>
      </AboutTeaser>
      <TeamTeaser>
        <ContentWrapper side="left">
          <H2>{t(messages['TeamTeaser.title'])}</H2>
          <Paragraph>{t(messages['TeamTeaser.content'])}</Paragraph>
          <RoundedLinkButton to={{ pathname: Routes.Team }}>
            {t(messages['TeamTeaser.linkText'])}
          </RoundedLinkButton>
        </ContentWrapper>
      </TeamTeaser>
      <SearchSuggestions side="right">
        <H2>
          <TextWithLineBreaks text={t(messages['SuggestedTerms.title'])} />
        </H2>
        <TeaserList>
          {suggestedTerms.map((item, idx) => (
            <TeaserListItem key={idx}>
              <TeaserLink to={{ pathname: item.url }}>{item.term}</TeaserLink>
            </TeaserListItem>
          ))}
        </TeaserList>
      </SearchSuggestions>
    </HomepageGrid>
  );
}

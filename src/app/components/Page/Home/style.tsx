import styled from 'styled-components/macro';
import { media, font_SuggestedTerm, font_H2 } from '../../../../styles';
import { pageContentPadding } from '../../PageLayout/style';
import { RouterLink } from '../../Link';
import { Grid } from '../../Grid';
import { LI, P, UL } from '../../HTML';
import { ContentWrapper, ContentWrapperProps } from '../ContentWrapper';

export const H1 = styled.h1`
  ${font_H2};
  margin: 0 auto ${props => props.theme.spacing._11}rem;
  ${media.desktop_up`
    margin: 0 auto ${props => props.theme.spacing._16}rem;
  `}
`;
export const H2 = styled.h2`
  ${font_H2};
  margin: 0 auto ${props => props.theme.spacing._11}rem;
  ${media.desktop_up`
    margin: 0 auto ${props => props.theme.spacing._16}rem;
  `}
`;
export const Paragraph = styled(P)`
  margin-bottom: ${props => props.theme.spacing._34}rem;
  ${media.desktop_up`
    margin-bottom: ${props => props.theme.spacing._24}rem;
  `};
`;

export const HomepageGrid = styled(Grid)`
  min-height: 100%;

  grid-template-columns: 1fr;
  -ms-grid-columns: 1fr;

  grid-template-areas:
    'homePage--about'
    'homePage--team'
    'homePage--search-suggestions';

  ${media.tablet_portrait_up`
    grid-template-rows: auto;
    -ms-grid-rows: auto;

    grid-template-columns: 1fr 1fr;
    -ms-grid-columns: 1fr 1fr;
    grid-column-gap: 0;

    grid-template-areas:
    'homePage--about homePage--search-suggestions'
    'homePage--team homePage--search-suggestions';
  `};

  ${media.desktop_up`
    grid-column-gap: 0;
  `};
`;

export const AboutTeaser = styled.div`
  grid-area: homePage--about;
  ${pageContentPadding};
  background-color: ${props => props.theme.color.light_green};
  padding-top: ${props => props.theme.spacing._58}rem;
  padding-bottom: ${props => props.theme.spacing._62}rem;

  ${media.desktop_up`
    padding-top: ${props => props.theme.spacing._80}rem;
  `};
`;

export const TeamTeaser = styled.div`
  grid-area: homePage--team;
  ${pageContentPadding};
  background-color: ${props => props.theme.color.light_blue};
  padding-top: ${props => props.theme.spacing._55}rem;
  padding-bottom: ${props => props.theme.spacing._55}rem;

  ${media.tablet_portrait_up`
    padding-top: ${props => props.theme.spacing._60}rem;
  `};

  ${media.desktop_up`
    padding-top: ${props => props.theme.spacing._40}rem;
  `};
`;

export const SearchSuggestions = styled(ContentWrapper)<ContentWrapperProps>`
  grid-area: homePage--search-suggestions;
  max-width: unset;
  ${pageContentPadding};
  padding-top: ${props => props.theme.spacing._40}rem;
  padding-bottom: ${props => props.theme.spacing._50}rem;

  ${media.tablet_portrait_up`
    padding-top: ${props => props.theme.spacing._58}rem;
  `};

  ${media.desktop_up`
    padding-top: ${props => props.theme.spacing._80}rem;
  `};

  h1 {
    margin-bottom: ${props => props.theme.spacing._30}rem;
  }
`;

export const TeaserList = styled(UL)``;

export const TeaserListItem = styled(LI)`
  margin-bottom: ${props => props.theme.spacing._10}rem;
`;

export const TeaserLink = styled(RouterLink)`
  ${font_SuggestedTerm}
`;

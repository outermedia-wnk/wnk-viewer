import { _t } from '../../../../utils/messages';

const scope = 'Page.Homepage';

export const messages = {
  'AboutTeaser.title': _t(`${scope}.AboutTeaser.title`),
  'AboutTeaser.content': _t(`${scope}.AboutTeaser.content`),
  'AboutTeaser.linkText': _t(`${scope}.AboutTeaser.linkText`),

  'SuggestedTerms.title': _t(`${scope}.SuggestedTerms.title`),

  'TeamTeaser.title': _t(`${scope}.TeamTeaser.title`),
  'TeamTeaser.content': _t(`${scope}.TeamTeaser.content`),
  'TeamTeaser.linkText': _t(`${scope}.TeamTeaser.linkText`),
};

import React from 'react';
import { useTranslation } from 'react-i18next';
import { H1 } from '../../HTML';
import { Wrapper } from './style';

export function NotFound() {
  const { t } = useTranslation();
  return (
    <Wrapper>
      <H1>{t('Page.NotFound.message')}</H1>
    </Wrapper>
  );
}

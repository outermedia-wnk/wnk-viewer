import styled from 'styled-components/macro';
import { pageContentLayout } from '../../PageLayout/style';

export const Wrapper = styled.div`
  ${pageContentLayout};
  padding-top: ${props => props.theme.spacing._80}rem;
`;

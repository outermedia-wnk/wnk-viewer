import styled, { css } from 'styled-components/macro';
import {
  font_Teaser,
  font_TeaserBuch,
  font_TeaserLight,
  media,
} from '../../../../styles';
import { H2, P } from '../../HTML';
import { Link } from '../../Link';
import { pageContentPadding } from '../../PageLayout/style';

export interface ContentWrapperProps {
  readonly side: 'left' | 'right';
}
export const ContentWrapper = styled.div<ContentWrapperProps>`
  ${media.desktop_up`
    // -5rem removes the padding for desktop
    max-width: calc(1280px / 2 - 5rem);
    ${props =>
      props.side &&
      css`
      margin-${props.side}: auto;
    `};
  `};
`;

const Content = styled.div`
  ${pageContentPadding};
  padding-top: ${props => props.theme.spacing._40}rem;
  padding-bottom: ${props => props.theme.spacing._28}rem;
  ${media.tablet_portrait_up`
    padding-top: ${props => props.theme.spacing._80}rem;
    padding-bottom: ${props => props.theme.spacing._40}rem;
  `};
`;

interface ContentLeftProps {
  bgColor?: 'green' | 'blue' | 'yellow';
}
export const ContentLeft = styled(Content)<ContentLeftProps>`
  grid-area: left;
  background-color: ${props =>
    props.bgColor
      ? (props.bgColor === 'green' && props.theme.color.light_green) ||
        (props.bgColor === 'blue' && props.theme.color.light_blue) ||
        (props.bgColor === 'yellow' && props.theme.color.light_yellow)
      : props.theme.color.white};
`;
export const ContentRight = styled(Content)`
  grid-area: right;
`;

export const Title = styled(H2)`
  ${media.tablet_portrait_up`
    margin-bottom: ${props => props.theme.spacing._28}rem;
  `};
`;

export const TeaserTitle = styled(H2)`
  ${media.tablet_portrait_up`
    margin-bottom: ${props => props.theme.spacing._18}rem;
  `};
`;

export const TeaserText = styled(P)`
  ${font_Teaser};
`;

export const TeaserTextLight = styled(P)`
  ${font_TeaserLight};
`;

export const TeaserTextBuch = styled(P)<{ $noMarginBottom?: boolean }>`
  ${font_TeaserBuch};
  ${props =>
    props.$noMarginBottom &&
    css`
      margin-bottom: 0;
    `}
`;

export const TextLink = styled(Link)`
  cursor: pointer;
`;

export const Department = styled.div`
  ${font_TeaserBuch};
  font-style: normal;
`;

export const Address = styled.address<{ $doubleAddress?: boolean }>`
  ${font_TeaserBuch};
  font-style: normal;
  ${props =>
    props.$doubleAddress &&
    css`
      margin-top: ${props.theme.spacing._6}rem;
    `};
`;

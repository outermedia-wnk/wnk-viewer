import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TreeNode } from '../../api/xtree';
import { selectNodeById } from '../../features/Tree';
import { StringUtils } from '../../model/StringUtils';
import { VocabularyItem } from '../../model/vocabularyItems/VocabularyItem';

interface Props {
  id: string;
  vocab?: VocabularyItem;
}

export function TermWithQualifierOrEntityType(props: Props) {
  const { t } = useTranslation();
  const node: TreeNode | undefined = useSelector(selectNodeById(props.id));

  if (node) {
    let title: string = StringUtils.termWithQualifier(
      node.preferredTerm,
      node.qualifier,
    );
    title = StringUtils.termWithEntityType(title, node.entityType, t);
    return <>{title}</>;
  }

  if (props.vocab) {
    let title: string = props.vocab.title;
    title = StringUtils.termWithEntityType(title, props.vocab.type, t);
    return <>{title}</>;
  }

  return null;
}

import React from 'react';
import { IconLinkArrow } from '../../Icon';
import { LinkWithIcon, NoWrap } from './styled';

interface Props {
  url: string;
  text: string;
  linkProps?: any;
}

export function ExternalLink(props: Props) {
  const textWithoutLastWord: string = removeLastWord(props.text);
  const lastWord: string = getLastWord(props.text);

  return (
    <LinkWithIcon
      {...props.linkProps}
      href={props.url}
      target="_blank"
      rel="noreferrer"
    >
      <span>{textWithoutLastWord}</span>
      {(textWithoutLastWord.endsWith(' ') || lastWord.startsWith(' ')) && ' '}
      <NoWrap>
        {lastWord}
        <IconLinkArrow aria-hidden={true} />
      </NoWrap>
    </LinkWithIcon>
  );
}

function removeLastWord(text: string) {
  return text.substring(0, Math.max(0, text.lastIndexOf(' ')));
}
function getLastWord(text: string) {
  return text.substring(Math.max(0, text.lastIndexOf(' ')), text.length);
}

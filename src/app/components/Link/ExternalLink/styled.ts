import styled from 'styled-components/macro';
import { Link } from '../index';

export const LinkWithIcon = styled(Link)`
  svg {
    margin-left: ${props => props.theme.spacing._5}rem;
  }
`;

export const NoWrap = styled.span`
  display: inline-block;
  text-decoration: underline;
  white-space: nowrap;
`;

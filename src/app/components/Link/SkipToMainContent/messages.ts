import { _t } from '../../../../utils/messages';

const scope = 'Link.SkipToMainContent';

export const messages = {
  linkMainText: _t(`${scope}.main.linkText`),
  linkSearchText: _t(`${scope}.search.linkText`),
};

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import { useSelectedVocab } from '../../../hooks/useSelectedVocab';
import { VocabularyItem } from '../../../model/vocabularyItems/VocabularyItem';
import { P } from '../../HTML';
import { ErrorWrapper } from './style';

interface Props {
  error?: Error | null;
}

export function TreeError(props: Props) {
  const { id } = useParams();
  const vocab: VocabularyItem | null = useSelectedVocab();

  if (!props.error) {
    return null;
  }

  const isValidId: boolean = !!id;
  const vocabExists: boolean = !!vocab && isValidId && vocab.id === id;

  return (
    <ErrorWrapper>
      {vocabExists && <BackendSyncError />}
      {!vocabExists && isValidId && <IdNotFoundError />}
      {!vocabExists && !isValidId && <DefaultError />}
    </ErrorWrapper>
  );
}

function BackendSyncError() {
  const { t } = useTranslation();
  return (
    <>
      <h2>{t('Page.Tree.Error.backend.not.synchronised.title')}</h2>
      <P>{t('Page.Tree.Error.backend.not.synchronised.message')}</P>
    </>
  );
}

function IdNotFoundError() {
  const { t } = useTranslation();
  return (
    <>
      <h2>{t('Page.Tree.Error.not.found.title')}</h2>
      <P>{t('Page.Tree.Error.not.found.message')}</P>
    </>
  );
}

function DefaultError() {
  const { t } = useTranslation();
  return (
    <>
      <h2>{t('Page.Tree.Error.generic.title')}</h2>
      <P>{t('Page.Tree.Error.generic.message')}</P>
    </>
  );
}

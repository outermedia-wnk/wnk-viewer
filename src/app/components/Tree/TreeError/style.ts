import styled from 'styled-components/macro';
import { font_H2 } from '../../../../styles';

export const ErrorWrapper = styled.div`
  h2 {
    ${font_H2};
  }
`;

import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import {
  NodesMap,
  selectError,
  selectIsTreeLoading,
  selectNodesMap,
  useTreeSlice,
} from '../../features/Tree';
import { TreeBuilder } from '../../model/tree/TreeBuilder';
import { TreeBranch } from './TreeBranch';
import { TreeError } from './TreeError';

export function Tree() {
  const { actions } = useTreeSlice();
  const dispatch = useDispatch();
  const nodesMap: NodesMap = useSelector(selectNodesMap);
  const error: Error | null = useSelector(selectError);
  const isLoading: boolean = useSelector(selectIsTreeLoading);
  const { id } = useParams();
  const hasNodes = Object.keys(nodesMap).length > 0;
  const treeHasId = nodesMap.hasOwnProperty(id ?? '');
  const isMissingHierarchyParentIds =
    treeHasId &&
    nodesMap[id].superordinateRelation &&
    nodesMap[id].superordinateRelation.some(item => {
      if (
        !!item.typeOfHierarchicalRelation &&
        !nodesMap.hasOwnProperty(item.id)
      ) {
        console.log('missing id', item.id);
        return true;
      }
      return false;
    });
  const missingHierarchyParentIds: string[] = useMemo(() => {
    return treeHasId && nodesMap[id].superordinateRelation
      ? nodesMap[id].superordinateRelation
          .filter(
            item =>
              !!item.typeOfHierarchicalRelation &&
              !nodesMap.hasOwnProperty(item.id),
          )
          .map(item => item.id)
      : [];
  }, [id, nodesMap, treeHasId]);

  useEffect(() => {
    return () => {
      dispatch(actions.reset());
    };
  }, [actions, dispatch]);

  useEffect(() => {
    if (isLoading || error) {
      return;
    }
    if (id && !treeHasId) {
      dispatch(actions.loadTree({ id }));
    } else if (!hasNodes) {
      // load roots only if there's no ID, to prevent unnecessary calls
      dispatch(actions.loadTree({}));
    } else if (id && treeHasId) {
      dispatch(actions.selectNode({ id }));
      if (missingHierarchyParentIds.length) {
        dispatch(
          actions.loadTree({ id: id, missingIds: missingHierarchyParentIds }),
        );
      }
    }
  }, [
    actions,
    dispatch,
    error,
    hasNodes,
    id,
    isLoading,
    isMissingHierarchyParentIds,
    missingHierarchyParentIds,
    treeHasId,
  ]);

  if (error) {
    return <TreeError error={error} />;
  }

  if (!hasNodes) {
    return null;
  }

  const tree: TreeBuilder = new TreeBuilder(nodesMap);
  return (
    <div>
      <TreeBranch branchId="root" tree={tree} ids={tree.roots} parentIds={[]} />
    </div>
  );
}

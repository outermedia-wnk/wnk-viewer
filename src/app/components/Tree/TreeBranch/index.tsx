import React from 'react';
import { TreeBuilder } from '../../../model/tree/TreeBuilder';
import { TreeItem } from '../TreeItem';
import { hashCode } from '../../../../utils/hashCode';

interface TreeBranchProps {
  tree: TreeBuilder;
  ids: string[];
  parentIds: string[];
  branchId: string;
}

export function TreeBranch(props: TreeBranchProps) {
  const hash = hashCode(`${props.ids.join(',')}-${props.parentIds.join(',')}`);
  const isRoot = props.branchId === 'root';
  const branchProps = {
    role: isRoot ? 'tree' : 'group',
  };
  if (isRoot) {
    branchProps['aria-labelledby'] = 'header-tree';
  }
  const idsAlphabeticallySorted = props.tree.sortIdsAlphabetically(props.ids);
  return (
    <div id={props.branchId} {...branchProps}>
      {idsAlphabeticallySorted.map((id: string) => (
        <TreeItem
          key={`${hash}-${id}`}
          tree={props.tree}
          nodeId={id}
          parentIds={props.parentIds}
        />
      ))}
    </div>
  );
}

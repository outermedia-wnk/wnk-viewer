import styled, { css } from 'styled-components/macro';
import { font_TreeItem, font_TreeItemFocused } from '../../../../styles';
import { IconButton } from '../../Button/IconButton';
import { RouterLink } from '../../Link';

interface TreeElementProps {
  level: number;
  $isRoot: boolean;
}
export const TreeElement = styled.div<TreeElementProps>`
  min-height: 11px;
  margin-left: ${props => props.level - 1 * props.theme.spacing._20}rem;
  display: flex;
  align-items: center;
  padding-bottom: ${props => props.theme.spacing._10}rem;
  ${props =>
    props.$isRoot &&
    css`
      width: 0;
      height: 0;
      padding: 0;
      margin: 0;
      min-height: 0;
    `}
`;

export const ToggleButton = styled(IconButton)`
  cursor: pointer;
  display: block;
  margin-right: ${props => props.theme.spacing._8}rem;
  &:hover svg path {
    fill: ${props => props.theme.color.link_blue};
  }
`;

export const DotWrapper = styled.div`
  margin-right: ${props => props.theme.spacing._6}rem;
  padding-left: ${props => props.theme.spacing._4}rem;
  padding-right: ${props => props.theme.spacing._4}rem;
`;

interface ItemLinkProps {
  $isFocused: boolean;
  $isOpen: boolean;
  $italic: boolean;
}
export const ItemLink = styled(RouterLink)<ItemLinkProps>`
  ${font_TreeItem};

  display: flex;
  align-items: center;
  text-decoration: none;

  ${props =>
    props.$isFocused &&
    css`
      ${font_TreeItemFocused};
      background-color: ${props.theme.color.dark_blue};
      padding: ${props.theme.spacing._3}rem ${props.theme.spacing._12}rem;
      &:hover {
        background-color: ${props.theme.color.link_blue};
        text-decoration: none;
        color: ${props.theme.color.white};
      }
    `};

  ${props =>
    props.$isOpen &&
    css`
      font-weight: ${props => props.theme.font.weight.bold};
    `};

  ${props =>
    props.$italic &&
    css`
      font-style: italic;
    `};

  svg {
    margin-right: ${props => props.theme.spacing._8}rem;
  }
`;

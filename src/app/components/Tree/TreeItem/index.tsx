import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TreeNode } from '../../../api/xtree';
import {
  actions,
  selectSelectedId,
  selectIsNodeExpanded,
} from '../../../features/Tree';
import { isStructureElement } from '../../../model/isStructureElement';
import { StringUtils } from '../../../model/StringUtils';
import { NodeInspector } from '../../../model/tree/NodeInspector';
import { TreeBuilder } from '../../../model/tree/TreeBuilder';
import { PermalinkBuilder } from '../../../routes/PermalinkBuilder';
import {
  TreeDotIcon,
  TreeToggleMinusIcon,
  TreeTogglePlusIcon,
} from '../../Icon';
import { TreeBranch } from '../TreeBranch';
import { DotWrapper, ItemLink, ToggleButton, TreeElement } from './style';

interface TreeItemProps {
  tree: TreeBuilder;
  nodeId: string;
  parentIds: string[];
}

export function TreeItem(props: TreeItemProps) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { nodeId, tree } = props;
  const isNodeExpanded = useSelector(selectIsNodeExpanded(nodeId));
  const selectedId = useSelector(selectSelectedId);
  const isSelected: boolean = selectedId === nodeId;

  const node: TreeNode = tree.getNodeById(nodeId);
  if (!node) {
    return null;
  }

  const title = StringUtils.termWithQualifier(
    node.preferredTerm,
    node.qualifier,
  );

  const hasChildren = node.childrenIds.length > 0;
  const path: string[] = [...props.parentIds, node.id];
  const url = PermalinkBuilder.build(nodeId);

  const handleToggle = () => {
    dispatch(actions.toggleNode({ id: nodeId }));
    if (!isNodeExpanded && node.childrenIds.some(nid => !tree.hasNode(nid))) {
      dispatch(actions.loadChildren({ id: nodeId }));
    }
  };

  const isRootNode = NodeInspector.isRootNode(nodeId);

  const level = Math.max(0, path.length - 1);

  let treeItemProps = {};
  if (hasChildren) {
    treeItemProps = {
      role: 'treeitem',
      'aria-expanded': isNodeExpanded,
      'aria-level': level,
    };
  }
  if (isRootNode) {
    treeItemProps = { 'aria-hidden': true };
  }

  return (
    <>
      <TreeElement
        key={path.join(',')}
        data-path={path.join(',')}
        level={level}
        {...treeItemProps}
        $isRoot={isRootNode}
      >
        {!isRootNode && hasChildren && (
          <ToggleButton
            onClick={handleToggle}
            aria-label={t(
              `Icon.TreeItem.${isNodeExpanded ? 'close' : 'open'}.ariaLabel`,
              { title },
            )}
            title={t(
              `Icon.TreeItem.${isNodeExpanded ? 'close' : 'open'}.tooltip`,
              { title },
            )}
            aria-expanded={isNodeExpanded}
            aria-controls={`branch-from-${nodeId}`}
          >
            {isNodeExpanded ? (
              <TreeToggleMinusIcon aria-hidden={true} aria-labelledby="title" />
            ) : (
              <TreeTogglePlusIcon aria-hidden={true} aria-labelledby="title" />
            )}
          </ToggleButton>
        )}
        {!hasChildren && (
          <DotWrapper>
            <TreeDotIcon
              aria-labelledby="title"
              title={t('Icon.TreeItem.termWithNoChildren.tooltip')}
            />
          </DotWrapper>
        )}

        {!isRootNode && (
          <ItemLink
            to={{ pathname: url }}
            $isFocused={isSelected}
            $isOpen={isNodeExpanded}
            $italic={isStructureElement(node.entityType)}
          >
            <span>
              {StringUtils.termWithEntityType(title, node.entityType, t)}
            </span>
          </ItemLink>
        )}
      </TreeElement>
      {isNodeExpanded && hasChildren && (
        <TreeBranch
          branchId={`branch-from-${nodeId}`}
          tree={tree}
          ids={node.childrenIds}
          parentIds={path}
        />
      )}
    </>
  );
}

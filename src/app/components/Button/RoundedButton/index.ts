import styled, { css } from 'styled-components/macro';
import { RouterLink } from '../../Link';

const buttonStyles = css`
  background-color: transparent;
  cursor: pointer;
  display: inline-block;
  box-sizing: border-box;
  flex-flow: row nowrap;
  border-radius: ${props => props.theme.spacing._50}rem;
  padding: ${props => props.theme.spacing._8}rem
    ${props => props.theme.spacing._17}rem ${props => props.theme.spacing._9}rem;
  border: 1px solid ${props => props.theme.color.black};
  color: ${props => props.theme.color.black};

  &:hover {
    background-color: ${props => props.theme.color.dark_blue};
    color: ${props => props.theme.color.white};
  }
`;

export const RoundedLinkButton = styled(RouterLink)`
  ${buttonStyles};
  text-decoration: none;
  &:hover {
    text-decoration: none;
  }
`;

import { _t } from '../../../../utils/messages';

const scope = 'Button.ContentViewSwitcher';

export const messages = {
  details: _t(`${scope}.details`),
  searchResults: _t(`${scope}.searchResults`),
  tree: _t(`${scope}.tree`),
};

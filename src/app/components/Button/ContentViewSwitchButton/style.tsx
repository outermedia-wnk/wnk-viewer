import styled, { css } from 'styled-components/macro';
import { media } from '../../../../styles';

interface ButtonProps {
  readonly isActive: boolean;
}

export const Button = styled.button<ButtonProps>`
  background-color: transparent;
  border: none;
  box-sizing: border-box;
  color: ${props => props.theme.color.black};
  cursor: pointer;
  display: inline-block;
  flex-flow: row nowrap;
  margin-right: ${props => props.theme.spacing._16}rem;
  padding: ${props => props.theme.spacing._8}rem
    ${props => props.theme.spacing._17}rem
    ${props =>
      props.isActive ? props.theme.spacing._20 : props.theme.spacing._9}rem;

  ${props =>
    props.isActive
      ? css`
          background-color: ${props.theme.color.white};
          color: ${props.theme.color.black};
          font-weight: ${props.theme.font.weight.bold};
        `
      : css`
          background-color: ${props.theme.color.dark_blue_80_percent};
          color: ${props.theme.color.white};
        `};

  ${media.mobile_tiny`
    padding-left: ${props => props.theme.spacing._5}rem;
    padding-right: ${props => props.theme.spacing._5}rem;
    font-size: ${props => props.theme.font.size._14}rem;
  `}
`;

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import {
  actions,
  ContentViewType,
  selectCurrentView,
} from '../../../features/ContentViewSwitcher';
import { Button } from './style';
import { messages } from './messages';

interface Props {
  view: string;
}

export function ContentViewSwitchButton(props: Props) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const currentView = useSelector(selectCurrentView);

  const handleClick = evt => {
    evt.preventDefault();
    if (props.view === ContentViewType.Details) {
      dispatch(actions.viewDetails());
    }
    if (props.view === ContentViewType.SearchResults) {
      dispatch(actions.viewSearchResults());
    }
    if (props.view === ContentViewType.Tree) {
      dispatch(actions.viewTree());
    }
  };

  return (
    <Button onClick={handleClick} isActive={currentView === props.view}>
      {t(messages[props.view])}
    </Button>
  );
}

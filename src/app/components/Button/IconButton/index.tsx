import styled from 'styled-components/macro';

export const IconButton = styled.button`
  border: none;
  background: transparent;
  cursor: pointer;
  display: block;
  margin: 0;
  padding: 0;
`;

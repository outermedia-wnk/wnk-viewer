import { TreeNode } from '../../api/xtree';

export type NodesMap = Record<string, TreeNode>;

export interface TreeState {
  readonly isLoading: boolean;
  readonly error: Error | null;
  readonly nodesMap: NodesMap;
  readonly requestedIds: string[];
  readonly expandedNodes: string[];
  readonly selectedId: string | null;
}

import ConfigReducer from './slice';
export default ConfigReducer;
export * from './selectors';
export * from './slice';
export * from './types';

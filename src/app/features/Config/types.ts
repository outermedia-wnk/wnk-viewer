export interface ConfigState {
  readonly xTreeApi: {
    readonly url: string;
    readonly vocabulary: string;
    readonly username: string;
    readonly password: string;
    readonly formLoginName: string;
    readonly formLoginId: string;
  };
  readonly matomo: {
    readonly url: string;
    readonly siteId: number;
  };
}

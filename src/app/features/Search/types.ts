import { XTreeJsonResponse, NormalizerResponse } from '../../model/normalizer';

export type IDsMap = Record<string, any>;
export type IDsList = string[];

export interface SearchState {
  isLoading: boolean;
  request: SearchRequest;
  response?: SearchResponse;
  data: {
    idsList: string[];
    mapById: IDsMap;
  };
}

export interface SearchRequest {
  readonly term?: string;
  readonly id?: string;
}

export interface SearchResponse {
  apiResponse: XTreeJsonResponse;
  normalizedData: NormalizerResponse;
}

export interface AuthState {
  readonly isAuthenticated: boolean;
  readonly isLoading: boolean;
}

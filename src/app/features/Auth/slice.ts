import { createSlice } from '../../../utils/@reduxjs/toolkit'; // importing from `utils` is more type-safe
import { AuthState } from './types';

export const initialState: AuthState = {
  isAuthenticated: false,
  isLoading: false,
};

// The Immer library used by createSlice and createReducer will return an immutable state,
// so we can write code that "mutates" the state inside our reducer
const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login(state) {
      state.isLoading = true;
    },
    loginSuccess(state) {
      state.isLoading = false;
      state.isAuthenticated = true;
    },
    loginError(state) {
      state.isLoading = false;
      state.isAuthenticated = false;
    },
    logout(state) {
      state.isLoading = true;
    },
    logoutSuccess(state) {
      state.isLoading = false;
      state.isAuthenticated = false;
    },
    logoutError(state) {
      state.isLoading = false;
    },
  },
});

// Exports
export const { name: sliceKey, actions } = authSlice;
export default authSlice.reducer;

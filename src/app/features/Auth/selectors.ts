import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../../../types';
import { initialState } from './slice';

// First select the relevant part from the state
const selectAuth = (state: RootState) => state.auth || initialState;

export const selectIsAuthenticated = createSelector(
  [selectAuth],
  substate => substate.isAuthenticated,
);

export const selectIsLoading = createSelector(
  [selectAuth],
  substate => substate.isLoading,
);

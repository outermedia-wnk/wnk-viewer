export interface MobileMenuState {
  readonly isOpen: boolean;
}
